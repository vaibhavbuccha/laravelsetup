<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::get('verify-account/{id}', 'App\Http\Controllers\Front\UserController@accountConfirmation')->name('user.verify-account');

Route::get('/resetPassword/{token}', 'App\Http\Controllers\Front\UserController@resetPassword')->middleware('guest');

Route::group(['middleware' => 'prevent-back-history'],function(){
	Route::group(['namespace' => 'App\Http\Controllers'], function()
    {
		//Auth::routes();
		/* Start Custom Auth URL's */
			// Authentication Routes..

			Route::post('logout', 'Auth\LoginController@logout')->name('logout');

			// Registration Routes...
			Route::get('register', 'Auth\RegisterController@showRegistrationForm')->name('register');
			Route::post('register', 'Auth\RegisterController@register');

			// Password Reset Routes...
			Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
			Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
			Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
			Route::post('password/reset', 'Auth\ResetPasswordController@reset')->name('password.update');

			// Email Verification Routes...
			Route::get('email/verify', 'Auth\VerificationController@show')->name('verification.notice');
			Route::get('email/verify/{id}', 'Auth\VerificationController@verify')->name('verification.verify');
			Route::get('email/resend', 'Auth\VerificationController@resend')->name('verification.resend');

			// Admin Urls
			Route::get('admin/login', 'Adminauth\LoginController@showLoginForm')->name('adminLoginFrom');
			Route::post('admin/login', 'Adminauth\LoginController@adminLogin')->name('adminlogin');
			Route::post('admin/logout', 'Adminauth\LoginController@logout')->name('adminlogout');
			Route::get('/admin/password/reset','Adminauth\ForgotPasswordController@showForgotPasswordForm')->name('forgotpasswordform');
			Route::post('/admin/password/email','Adminauth\ForgotPasswordController@sendResetLinkEmail')->name('adminforgotpassword');
			Route::get('/admin/password/reset/{token}','Adminauth\ResetPasswordController@showResetPasswordForm')->name('showResetPasswordForm');
			Route::post('/admin/password/reset','Adminauth\ResetPasswordController@reset')->name('adminResetPassword');

			/* END Custom Auth URL's */
    });


	Route::group(['prefix' => 'admin', 'namespace' => 'App\Http\Controllers\Admin', 'middleware' => ['admin']], function()
	{
		/*DASHBOARD CONTROLLER*/
		Route::get('/','DashboardController@index');
		Route::get('dashboard', 'DashboardController@index')->name('admin.dashboard');

		Route::resource('user-profile', 'ProfileController');
        Route::post('/changePassword/{id}','ProfileController@changePassword')->name('changePassword');

        /*NOTIFICATION MESSAGE CONTROLLER*/
        Route::get('notification-messages/getAllData', 'NotificationMessageController@getAllData');
        Route::resource('notification-messages', 'NotificationMessageController');

        /*REGISTERED USER CONTROLLER*/
        Route::get('user-management/getAllData', 'UserController@getalldata');
        Route::get('exportUserData', 'UserController@exportUser');
        Route::get('activeUser/{id}', 'UserController@activeUser');
        Route::get('inactiveUser/{id}', 'UserController@inactiveUser');
        Route::resource('user-management', 'UserController');




	});

});