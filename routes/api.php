<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['prefix' => 'v1', 'namespace' => 'App\Http\Controllers\API\V1'], function()
{ 
	//UserControler
	Route::post('signUp', 'UserController@signUp');
	Route::post('login', 'UserController@login');
	Route::post('forgotPassword', 'UserController@forgotPassword');

	Route::group(['middleware' => ['jwt-auth']], function () {
		Route::post('logout', 'UserController@logout');
	});
});
