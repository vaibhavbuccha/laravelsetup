$(document).on("click",".viewInformation" ,function(e) {
  e.preventDefault();
  $.LoadingOverlay("show", {
    image: "/themes/loader.gif",
  });
  var dataId = $(this).data('id');
  var dataURL = $(this).data('url');
   $.ajax({
        type: "GET", 
        url:  dataURL, 
        success: function (data) {
          $('#dataInfoModal').html(data);
          $("#dataInfoModal").modal('show');
         $.LoadingOverlay("hide");
        },
    });

  $(document).on("click", "#deleteDetails", function (event) {
    event.preventDefault();
    $("#modalConfirmDelete").modal('show');
  });

});