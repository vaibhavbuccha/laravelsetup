<!DOCTYPE html>
<html class="no-js css-menubar" lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
  
  <title>{{Config::get('constant.APP_NAME')}} - Admin</title>

  <link rel="shortcut icon" href="{{URL(Config::get('constant.LOGO_FAVICON'))}}" />

  <!-- Stylesheets -->
  {{ Html::style('themes/admin/assets/global/css/bootstrap.minfd53.css') }}
  {{ Html::style('themes/admin/assets/global/css/bootstrap-extend.minfd53.css') }}
  {{ Html::style('themes/admin/assets/css/site.minfd53.css') }}
  {{ Html::style('themes/admin/assets/examples/css/pages/login-v2.minfd53.css') }}
  {{ Html::style('themes/admin/assets/global/fonts/material-design/material-design.minfd53.css') }}
  {{ Html::style('themes/admin/assets/global/fonts/brand-icons/brand-icons.minfd53.css') }}
  {{ Html::style('themes/admin/assets/global/vendor/formvalidation/formValidation.minfd53.css') }}
  <link rel='stylesheet' href="https://fonts.googleapis.com/css?family=Roboto:400,400italic,700">


<!--   <link rel="stylesheet" href="../../global/vendor/animsition/animsition.minfd53.css?v4.0.1">
  <link rel="stylesheet" href="../../global/vendor/asscrollable/asScrollable.minfd53.css?v4.0.1">
  <link rel="stylesheet" href="../../global/vendor/switchery/switchery.minfd53.css?v4.0.1">
  <link rel="stylesheet" href="../../global/vendor/intro-js/introjs.minfd53.css?v4.0.1">
  <link rel="stylesheet" href="../../global/vendor/slidepanel/slidePanel.minfd53.css?v4.0.1">
  <link rel="stylesheet" href="../../global/vendor/flag-icon-css/flag-icon.minfd53.css?v4.0.1">
  <link rel="stylesheet" href="../../global/vendor/waves/waves.minfd53.css?v4.0.1"> -->
<style type="text/css">
  .form-material.has-warning .form-control-label, .form-control-feedback, .error {
      color: #f44336;
  }
  .form-control-feedback{
    margin-left: 15px;
  }
  @media only screen and (min-width: 768px) {
    .page-login-v2 .page-login-main{
      height: 100vh;
    }
  }
</style>

  <!--[if lt IE 9]>
    {{ Html::script('themes/admin/assets/global/vendor/html5shiv/html5shiv.min.js') }}
    <![endif]-->

  <!--[if lt IE 10]>
    {{ Html::script('themes/admin/assets/global/vendor/media-match/media.match.min.js') }}
    {{ Html::script('themes/admin/assets/global/vendor/respond/respond.min.js') }}
    <![endif]-->

  <!-- Scripts -->
  {{ Html::script('themes/admin/assets/global/vendor/breakpoints/breakpoints.minfd53.js') }}
  <script>
    Breakpoints();
  </script>
</head>
<body class="animsition page-login-v2 layout-full page-dark">
  <!--[if lt IE 8]>
        <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->


  <!-- Page -->
  <div class="page" data-animsition-in="fade-in" data-animsition-out="fade-out">
    <div class="page-content text-center">
      <div class="page-brand-info">
        <div class="brand">
        
            <img class="brand-img" src="{{url(Config::get('constant.ADMIN.LOGIN_LOGO'))}}" alt="{!!Config::get('constant.APP_NAME')!!}">
        </div>
      </div>

      <div class="page-login-main form-horizontal">
        <div class="brand hidden-md-up">
          <img class="brand-img" src="{{url(Config::get('constant.ADMIN.LOGIN_LOGO'))}}" alt="{!!Config::get('constant.APP_NAME')!!}">
        </div>
        <h3 class="font-size-24">Reset Password</h3>
        
        <form action="{{ route('adminforgotpassword') }}" method="post" id="adminreset">
         @csrf 
         
          <div class="form-group row form-material text-left">
            <?php 
              if(session('status'))
              {
              ?>
                <div class="col-xl-12 text-left alert alert-success" role="alert">
                  {{ session('status') }}
                </div>
              <?php } ?>

            <label class="col-xl-12 text-left form-control-label">Email
              <span class="required">*</span>
            </label>
            <div class=" col-xl-12">
              <input type="email" class="form-control empty @error('email') is-invalid @enderror" id="inputEmail" name="email" value="{{ old('email') }}" autocomplete="off" required autofocus>
              @error('email')
                <label id="email-error" class="error" for="email">{{ $message }}</label>
              @enderror
            </div>
          </div>
          <div class="form-group clearfix">
            <a class="float-right" href="{{url('/admin')}}">Go to login</a>
          </div>
          <button type="submit" class="btn btn-primary btn-block" id="kt_sign_in_submit">Send Password Reset Link</button>
        </form>

      </div>

    </div>
  </div>
  <!-- End Page -->


  <!-- Core  -->
  {{ Html::script('themes/admin/assets/global/vendor/babel-external-helpers/babel-external-helpersfd53.js') }}
  {{ Html::script('themes/admin/assets/global/vendor/jquery/jquery.minfd53.js') }}
  {{ Html::script('themes/admin/assets/global/vendor/popper-js/umd/popper.minfd53.js') }}
  {{ Html::script('themes/admin/assets/global/vendor/bootstrap/bootstrap.minfd53.js') }}
  {{ Html::script('themes/admin/assets/global/vendor/animsition/animsition.minfd53.js') }}
  {{ Html::script('themes/admin/assets/global/vendor/jquery-placeholder/jquery.placeholder.minfd53.js') }}
  {{ Html::script('themes/admin/assets/global/vendor/formvalidation/formValidation.minfd53.js') }}
  {{ Html::script('themes/admin/assets/global/vendor/formvalidation/framework/bootstrap4.minfd53.js') }}

  <script type="text/javascript">
    $(document).ready(function() {
      $('#adminreset')
        .formValidation({
          framework: 'bootstrap4',
          icon: {
              valid: 'glyphicon glyphicon-ok',
              invalid: 'glyphicon glyphicon-remove',
              validating: 'glyphicon glyphicon-refresh'
          },
          fields: {
            email: { 
              validators: { 
                notEmpty: { 
                  message: "Email address is required." 
                }, 
                emailAddress: { 
                  message: "Enter valid email." 
                }
              } 
            },
          }
        });
      });
    
    </script>
</body>

</html>