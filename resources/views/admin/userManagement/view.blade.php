<div class="modal-dialog modal-dialog-slideout" role="document">
  <div class="modal-content">
    <div class="modal-header slidePanel-header bg-light-green-600">
      <div class="overlay-top overlay-panel overlay-background bg-light-green-600">
        <div class="slidePanel-actions btn-group btn-group-flat" aria-label="actions" role="group">
          <button type="button" class="btn btn-pure icon md-edit subtask-toggle custom-nav-buttons" id="openEditForm" aria-hidden="true" data-target="#editForm" target-discard="#viewForm" title="Edit"></button>
          <button type="button" class="btn btn-pure icon md-delete subtask-toggle" aria-hidden="true" id="deleteDetails" title="Delete"></button>
          <button type="button" class="btn btn-pure slidePanel-close icon md-close" data-dismiss="modal" aria-hidden="true" title="Close"></button>
        </div>
        <h5 class="stage-name taskboard-stage-title">View Details</h5>
      </div>
    </div>
    <div class="modal-body custom-nav-tabs">
      <div id="viewForm" class="active">
        <table class="table" >
          <tr>
            <td width="25%"><label class="text-bold">First Name</label></td>
            <td>{{$user->name}}</td>
          </tr>
          <tr>
            <td width="25%"><label class="text-bold">Last Name</label></td>
            <td>{{$user->last_name}}</td>
          </tr>
          <tr>
            <td width="25%"><label class="text-bold">Email</label></td>
            <td>{{$user->email}}</td>
          </tr>
          <tr>
            <td width="25%"><label class="text-bold">Date Of Bith</label></td>
            <td>{{dateFormat($user->dob)}}</td>
          </tr>
          <tr>
            <td width="25%"><label class="text-bold">Country</label></td>
            <td>{{$user->country}}</td>
          </tr>
          <tr>
            <td width="25%"><label class="text-bold">City</label></td>
            <td>{{$user->city}}</td>
          </tr>
          <tr>
            <td width="25%"><label class="text-bold">Status</label></td>
            <td>
              @if($user->is_active == 1)
              <a href="{{ url('/admin/inactiveUser/'.$user->id)}}" class="btn btn-danger">Inactive</a> 
              @else
              <a href="{{ url('/admin/activeUser/'.$user->id)}}" class="btn btn-success">Active</a> 
            </td>
            @endif
          </tr>
          <tr>
            <td width="25%"><label class="text-bold">Profile Pic</label></td>
            <td>
              <img width="100" class="img-fluid" src="{{($user->profile_pic) ? url('uploads/profile_pics/'.$user->profile_pic) : url('uploads/profile_pics/default-avatar.png')}}">
            </td>
          </tr>
        </table>  
      </div>
      <div id="editForm" class="hide">
        <div class="box-body">
          {{ Form::model($user, ['route' => ['user-management.update', $user->id], 'method' => 'put', 'role' => 'form','enctype'=>'multipart/form-data','id'=>'userEditForm'] ) }}
          <div class="col-md-12">
            <div class="row">
              <div class="form-group form-material col-md-6 @error('name') has-danger @enderror">
                <label class="form-control-label" for="inputBasicFirstName">First Name <span class="text-danger">*</span></label>
                  <input type="text" class="form-control" placeholder="First name" value="{{ucfirst($user->name)}}" name="name" autocomplete="off" required />
                  @error('name')
                  <label class="form-control-label" for="name">{{ $message }}</label>
                  @enderror
              </div>
              <div class="form-group form-material col-md-6 @error('last_name') has-danger @enderror">
                <label class="form-control-label" for="inputBasicLastName">Last Name <span class="text-danger">*</span></label>
                  <input type="text" class="form-control" placeholder="Last name" value="{{ucfirst($user->last_name)}}" name="last_name" autocomplete="off" required />
                  @error('last_name')
                  <label class="form-control-label" for="name">{{ $message }}</label>
                  @enderror
              </div>
            </div>
           </div> 
           <div class="col-md-12">
            <div class="row">
              <div class="form-group form-material col-md-6 @error('country') has-danger @enderror">
                <label class="form-control-label" for="inputCountry">Country <span class="text-danger">*</span></label>
                  <input type="text" class="form-control" placeholder="Country" value="{{ucfirst($user->country)}}" name="country" autocomplete="off" required />
                  @error('country')
                  <label class="form-control-label" for="country">{{ $message }}</label>
                  @enderror
              </div>
              <div class="form-group form-material col-md-6 @error('city') has-danger @enderror">
                <label class="form-control-label" for="inputCity">City <span class="text-danger">*</span></label>
                  <input type="text" class="form-control" placeholder="City" value="{{ucfirst($user->city)}}" name="city" autocomplete="off" required />
                  @error('city')
                  <label class="form-control-label" for="city">{{ $message }}</label>
                  @enderror
              </div>
            </div>
           </div>
           <div class="col-md-12">
            <div class="row">
              <div class="form-group form-material col-md-6 @error('dob') has-danger @enderror">
                <label class="form-control-label" for="inputCity">Birthdate <span class="text-danger">*</span></label>
                  <input type="text" class="form-control userbirthdate" placeholder="Birthdate" value="{{ucfirst($user->dob)}}" name="dob" autocomplete="off" readonly required />
                  @error('dob')
                  <label class="form-control-label" for="dob">{{ $message }}</label>
                  @enderror
              </div>
              {{-- <div class="form-group form-material col-md-6">
                {{ Form::label('Birthdate','Birthdate') }} <span class="red">*</span>
                <div class="no-padding">
                  {{ Form::text('dob',null,['class'=>'form-control userbirthdate','placeholder'=>'Enter Birthdate ','autocomplete'=>'off', "data-plugin"=>"datepicker",'readonly'],old('dob'))}}
                </div>
              </div> --}}
              <div class="form-group form-material col-md-6" data-plugin="">
                <label class="form-control-label" for="inputGroupFile01">Profile Pic</label>
                <input type="text" class="form-control custom-file-label" placeholder="Browse.." readonly="" />
                <input type="file" accept="image/png, image/jpeg, image/jpg, image/gif" id="inputGroupFile01" name="profile_pics" />
                <label style="color: #dd4b39 !important;font-weight: normal !important;width: 100%;" id="feed_image_error"></label>
                <br>
                <div id='img_contain' class="col-lg-12 col-md-12 col-sm-12">
                  <img id="profile-pics-preview" align='middle' src="{{($user->profile_pic) ? url('uploads/profile_pics/'.$user->profile_pic) : url('uploads/profile_pics/default-avatar.png')}}" alt="your image" width="100px" />
                </div> 
                <input type="hidden" name="user_profile_pic" id="imagebase64">
              </div>
            </div>
           </div>
            <br>
            <div class="form-group text-right">
              <button type="submit" class="btn btn-primary">Submit</button>
              <button type="button" class="btn btn-secondary custom-nav-buttons" target-discard="#editForm" data-target="#viewForm">Close</button>
            </div>
          {!! Form::close() !!}
        </div>
      
      </div>
    </div>
  </div>
</div>

<!-- /.delete Model-open -->
<div class="modal fade modal-fade-in-scale-up" id="modalConfirmDelete" aria-hidden="true"
                    aria-labelledby="exampleModalTitle" role="dialog" tabindex="-1">
    <div class="modal-dialog modal-sm modal-sm-new">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close deletemodel" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
          <h4 class="modal-title">Delete</h4>
        </div>
          <div class="modal-body">
            <p class="delete-conform-p">Are you sure you want to delete?</p>
          </div>
            {!! Form::open(array(
              'method' => 'DELETE',
              'route' => ['user-management.destroy', $user->id])) !!}
              <div class="modal-footer">
                <button type="button" class="btn btn-danger deletemodel" >Close</button>
                <button type="submit" class="btn btn-primary">Delete</button>
              </div>
          {!! Form::close() !!}
      </div>
    </div>
  </div>
<!-- /.delete Model-close -->

{{ Html::script('themes/admin/assets/js/form-validation.js') }}
  <script type="text/javascript">
    $(document).ready(function() {

      $('.userbirthdate').datepicker({
          todayBtn:'linked',
          format: 'yyyy-mm-dd',
          autoclose:true,
          endDate: "today"
       });


        $('.custom-nav-buttons').click(function(event){
          event.preventDefault();//stop browser to take action for clicked anchor
          //get displaying tab content jQuery selector
          var active_tab_selector = $(this).attr("target-discard");        
          //hide displaying tab content
          $(active_tab_selector).removeClass('active');
          $(active_tab_selector).addClass('hide');
                      
          //show target tab content
          var target_tab_selector = $(this).data("target");
          $(target_tab_selector).removeClass('hide');
          $(target_tab_selector).addClass('active');
        });

        if(!$('#image_demo').data('croppie')){
          $image_crop = $('#image_demo').croppie({
            enableExif: true,
            viewport: {
              width:350,
              height:350,
              // type:'square' //circle // rectangular
            },
            boundary:{
              width:600,
              height:400
            }    
          });
        }
        else{
          $('#image_demo').data('croppie').destroy();
          $image_crop = $('#image_demo').croppie({
            enableExif: true,
            viewport: {
              width:350,
              height:350,
              // type:'square' //circle // rectangular
            },
            boundary:{
              width:600,
              height:400
            }    
          });
        }

        $(document).on('change','#inputGroupFile01',function() {
          var name = $(this).attr('name');
          // var id_name= $(this).attr('id');
          // console.log('name = ' +name);
          var noImage = "{{asset('admin-theme/assets/images/default-img.png')}}";
          var ext = $(this).val().split('.').pop().toLowerCase();
          if ($.inArray(ext, ['png', 'jpg', 'jpeg']) == -1) {
              alert("Please select only image");
              return false;
          }
          else{
            //$("#file-error").attr("disabled", false);
              /* image crop */
              var reader = new FileReader();
              reader.onload = function (event) {
                $image_crop.croppie('bind', {
                  url: event.target.result
                }).then(function(){
                  // console.log('jQuery bind complete');
                });
              }
              reader.readAsDataURL(this.files[0]);
              $(".crop_image").attr('id',name);
              $('#insertimageModal').modal('show');

          }
          // return false;
          /* EOC image cropping */
        });

        $('.crop_image').click(function(event){

            var className = $(this).attr('id');
          $image_crop.croppie('result', {
            type: 'canvas',
            size: 'viewport'
          }).then(function(response){
            $("#profile-pics-preview").attr("src",response);
            $("#imagebase64").val(response);
            
            $('#insertimageModal').modal('hide');
            $("#feed_image_error").html("");
          });
        });

        $('.deletemodel').click(function(event){
          $('#modalConfirmDelete').modal('hide');
          });
    });
  </script>
