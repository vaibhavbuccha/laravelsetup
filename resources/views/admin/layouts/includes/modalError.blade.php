{{-- {{ Html::style('admin_theme/assets/css/custom.css') }} --}}
<div class="modal-dialog" id="myinformationModal">
  <div class="modal-content">
    <div class="modal-header">
      <h4 class="modal-title">Opps!!</h4>
      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">×</span>
    </button>
</div>
<div class="modal-body" id="informationModelBody"> 
    <div class="row">
        <div class="col-md-12 text-danger text-center">
            <div class="form-group">
                <i class="fa fa-exclamation-triangle fa-5x" aria-hidden="true"></i>
                <h5>Opps!! Something went wrong. Please try again.</h5>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 text-danger text-center">
            <div class="form-group">
                <button type="button" class="btn btn-danger btn-flat" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
</div>
</div> 

