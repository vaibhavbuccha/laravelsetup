<div class="site-menubar site-menubar-light">
    <div class="site-menubar-body">
      <div>
        <div>
          <ul class="site-menu" data-plugin="menu">
            <li class="site-menu-item @if(in_array('dashboard', Request::segments())) active @endif">
              <a href="{{route('admin.dashboard')}}">
                <i class="site-menu-icon md-view-dashboard" aria-hidden="true"></i>
                <span class="site-menu-title">Dashboard</span>
              </a>
            </li>
            <li class="site-menu-item @if(in_array('user-management', Request::segments())) active @endif">
              <a href="{{url('admin/user-management')}}">
                <i class="site-menu-icon md-accounts-list" aria-hidden="true"></i>
                <span class="site-menu-title">Users</span>
              </a>
            </li>
            <li class="site-menu-item has-sub @if(in_array('notification-messages', Request::segments())) active @endif">
              <a href="javascript:void(0)">
                <i class="site-menu-icon md-comment-list" aria-hidden="true"></i>
                <span class="site-menu-title">Messages</span>
              </a>
              <ul class="site-menu-sub">
                <li class="site-menu-item">
                  <a href="javascript:void(0)">
                    <span class="site-menu-title">Push Notification</span>
                  </a>
                </li>
                <li class="site-menu-item @if(in_array('notification-messages', Request::segments())) active @endif">
                  <a href="{{url('admin/notification-messages')}}">
                    <span class="site-menu-title">Notification Messages</span>
                  </a>
                </li>
              </ul>

          </ul>
        </div>
      </div>
    </div>
  </div>