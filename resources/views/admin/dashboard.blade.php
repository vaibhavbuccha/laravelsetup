@extends('admin.layouts.main')
@section('content')
<div class="page">
   <div class="page-header">
      <h1 class="page-title">Dashboard</h1>
    </div>
    <div class="page-content container-fluid">
      	<div class="row">
			<div class="col-xl-6 col-lg-12">
	          <div class="row">
	            <div class="col-lg-6">
	              <div class="card card-block p-25">
	                <div class="counter counter-lg">
	                  <span class="counter-number">60</span>
	                  <div class="counter-label text-uppercase">counters</div>
	                </div>
	              </div>
	            </div>
	            <div class="col-lg-6">
	              <div class="card card-block p-25">
	                <div class="counter counter-lg">
	                  <div class="counter-number-group">
	                    <span class="counter-number-related">-</span>
	                    <span class="counter-number">120</span>
	                  </div>
	                  <div class="counter-label text-uppercase">points</div>
	                </div>
	              </div>
	            </div>
	            <div class="col-lg-6">
	              <div class="card card-block p-25 bg-blue-600">
	                <div class="counter counter-lg counter-inverse">
	                  <div class="counter-label text-uppercase">score</div>
	                  <span class="counter-number">220</span>
	                </div>
	              </div>
	            </div>
	            <div class="col-lg-6">
	              <div class="card card-block p-25 bg-orange-600">
	                <div class="counter counter-lg counter-inverse">
	                  <div class="counter-label text-uppercase">earn</div>
	                  <div class="counter-number-group">
	                    <span class="counter-number-related">-</span>
	                    <span class="counter-number">90</span>
	                  </div>
	                </div>
	              </div>
	            </div>
	          </div>
	        </div>
	        <div class="col-xl-6 col-lg-12">
	          <div class="row">
	          	<div class="col-lg-6">
	              <div class="card card-block p-25 bg-green-600">
	                <div class="counter counter-lg counter-inverse">
	                  <div class="counter-label text-uppercase">score</div>
	                  <span class="counter-number">220</span>
	                </div>
	              </div>
	            </div>
	            <div class="col-lg-6">
	              <div class="card card-block p-25 bg-red-600">
	                <div class="counter counter-lg counter-inverse">
	                  <div class="counter-label text-uppercase">earn</div>
	                  <div class="counter-number-group">
	                    <span class="counter-number-related">-</span>
	                    <span class="counter-number">90</span>
	                  </div>
	                </div>
	              </div>
	            </div>
	            <div class="col-lg-6">
	              <div class="card card-block p-25">
	                <div class="counter counter-lg">
	                  <span class="counter-number">60</span>
	                  <div class="counter-label text-uppercase">counters</div>
	                </div>
	              </div>
	            </div>
	            <div class="col-lg-6">
	              <div class="card card-block p-25">
	                <div class="counter counter-lg">
	                  <div class="counter-number-group">
	                    <span class="counter-number-related">-</span>
	                    <span class="counter-number">120</span>
	                  </div>
	                  <div class="counter-label text-uppercase">points</div>
	                </div>
	              </div>
	            </div>
	          </div>
	        </div>
		</div>

	</div>
</div>
@stop

@section('footer_script')
@stop