<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
// use App\Models\Country;
// use App\Models\City;
use Redirect;
use Yajra\Datatables\Datatables;

class UserController extends Controller
{
    
    public function index()
    {
        $pageTitle = "User List";
        $users = User::orderBy('id', 'DESC')->where('is_active', '=', 1)->get();  
        // dd($users);
        return view('admin.userManagement.index', compact('users', 'pageTitle'));
    }


    public function getalldata()
    {
        // dd("hello");
        if(isset($_REQUEST['order'])){
            $users = User::where('is_active', '=', 1)->get();
        }
        else{
            $users = User::where('is_active', '=', 1)->orderBy('id', 'DESC')->get(); 
        } 
        
        
        return Datatables::of($users)
                
                ->addIndexColumn()
                ->editColumn('name', function($users){
                    
                        $fullName = trim($users->name.' '.$users->last_name);
                    
                    return $fullName;
                })->editColumn('email', function($users){

                    $email = isset($users->email)?trim($users->email):"N/A";

                    return $email;
                })->editColumn('dateofbirth', function($users){

                    $birthdate = isset($users->dob)?trim($users->dob):"N/A";

                    return $birthdate;
                })    
                ->setRowClass('viewInformation')
                ->setRowAttr([
                    'data-id' => function($user) {
                        return $user->id;
                    },
                    'data-url' => function($user) {
                        return url("admin/user-management/".$user->id);
                    },
                ])
                ->make(true);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    public function show($id)
    {
        $page = "User Details";

        $user = User::where('id',$id)->first();
        // $countries = Country::all();
        // $citys = City::where('country_id',$user->country)->orderBy('id','ASC')->get();
        if($user){
            return view('admin.userManagement.view', compact('user','page'));
        }
        else{
            return view('admin.layouts.includes.modalError');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    
    public function update(Request $request, $id)
    {
        // dd("update");
        $user = User::find($id);

        $page = "User Details";
        if($user){
            $updateData = [
                "name"                => $request->name,
                "last_name"           => $request->last_name,
            ]; 
            User::where("id",$id)->update($updateData);

            return Redirect::to("admin/user-management")->with("success","User details has been updated successfully.");
        }
        else{
            return Redirect::to("admin/user-management")->with("error","Something went wrong. Please try again!!");
        }
    }

   
    public function destroy($id)
    {
        $userDB = User::find($id);
        if($userDB)
        {
            $userDB->device_token = null;
            $userDB->save();
            //FIREBASE & STORE DELETE USER DETAIL's
            FirebaseHelper::deleteFireBaseUser($userDB->email);

            //FIREBASE UPDATE DELETED AT USER DOC
            if($userDB->is_profile_setup == 1){
                FirebaseHelper::updateFirebaseUserDeletedAtDocument($userDB->u_id);
            }
            //----
            if ($userDB->role != 3) {
                //FirebaseHelper::deleteFirestoreDocument($uid);
            }
            //----------------
            $userDB->delete();


            return redirect()->back()->with("success","User deleted successfully.");
        }
        else
        {
            return redirect()->back()->with("error","Opps!! Something went wrong. Please try again.");
        } 
    }


    /**
        * Author Name (JB)
        * Datetime (2020-05-06)
        * Export registered user in .csv file
    */
    public function exportUser($roleID){
        
        if($roleID == 0)
        {
            return Excel::download(new AllUsersExport, 'AllUsers-'.date('d-m-Y').'.csv');
        }
        elseif($roleID == 1)
        {
            return Excel::download(new InfluencerUsersExport, 'InfluencerUsers-'.date('d-m-Y').'.csv');
        }
        elseif($roleID == 2)
        {
            return Excel::download(new BusinessUsersExport, 'Brand-'.date('d-m-Y').'.csv');
        }
        elseif($roleID == 3)
        {
            return Excel::download(new AgencyUsersExport, 'AgencyUsers-'.date('d-m-Y').'.csv');
        }
    }

    public function freeSubscription(Request $request){
        $this->validate($request, [
            'user_id' => 'required',
            'subscription_date' => 'required'
        ],
        [
            'user_id.required'    => 'User Id is required field.',
            'subscription_date.required'           => 'Subscription date is required field.'
        ]);

        //dd($request->all());

        $user = User::find($request->user_id);

        Subscription::where("user_id",$request->user_id)->update(["status" => '4', "is_expired" => '1']);

        FirebaseHelper::updateUserExpireStatus($user, true);

        $start_date = Carbon::now()->format("Y-m-d H:i:s");
        $end_date = new Carbon($request->subscription_date);
        $end_date = $end_date->format("Y-m-d H:i:s");
        
        $plan = Plan::where("amount",'0')->first();

        $subscriptionDB = new Subscription();
        $subscriptionDB->user_id = $request->user_id;
        $subscriptionDB->plan_id = $plan->id;
        $subscriptionDB->start_date = $start_date;
        $subscriptionDB->end_date = $end_date;
        $subscriptionDB->status = 1;
        $subscriptionDB->is_expired = 0;
        $subscriptionDB->save();

        return redirect()->back()->with("success","Free subscription added successfully.");
    }
}
