<?php
return [
    "APP_NAME" => "mySplit.today",
    "LOGO_FAVICON" => "favicon.png",
    "FROM_EMAIL" => "noreply@clubcollab.com",
    "SUPPORT_EMAIL" => "support@clubcollab.com",
    "REPLAY_NAME" => "mySplit.today",
    "DATE_FORMAT" => "Y-m-d",
    "DATETIME_FORMAT" => "Y-m-d H:i:s",
    "ADS_TIMEZONE" => "Australia/Queensland",

    "CURRENCY_ICON" => "$",
    "ADMIN_CHARGE" => "5",

    "GOOGLE_MAP_API_KEY" => "AIzaSyBLMAE4YhzLHdK655ZfVJ5ZC2eiGBi0yAk",

    "STRIPE_SECRET_KEY" => "sk_test_rcSQKUkkiJRKdGmaQ5oPNhG900ChDjUnDd",
    "STRIPE_PUBLISH_KEY" => "pk_test_ulN8aShxsxX2K9hU3oleUnJ100D1FTUoBw",
    
    "ADMIN" => [
        "APP_NAME" => "mySplit.today",
        "HEADER_LOGO" => "themes/logos/logo.png",
        "LOGIN_LOGO" => "themes/logos/logo.png",
    ],

    "WEB" => [
        "APP_NAME" => "mySplit.today",
        "HEADER_LOGO" => "frontend/assets/images/logo/header-logo.svg",
        "FOOTER_LOGO" => "frontend/assets/images/logo/header-logo.svg",
    ],

    "EMAIL" => [
        "THEME_COLOR" => "#BDB18F",
        "THEME_FONT_COLOR" => "#FFF",
        "LOGO" => "frontend/assets/images/logo/header-logo.svg",
    ],
    
];
