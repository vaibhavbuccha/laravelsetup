<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('email')->unique();
            $table->string('last_name')->nullable();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->date('dob')->nullable();
            $table->string('country')->nullable();
            $table->string('city')->nullable();
            $table->string('referral_code')->nullable();
            $table->integer('is_active')->default('0')->comment("0=In Active, 1=Active");
            $table->integer('login_type')->default('0')->comment("0 = Normal, 1 = Facebook, 2 = Google");
            $table->integer('device_type')->default('0')->comment("0 = Web, 1 = iOS, 2 = Android");
            $table->string('device_token')->nullable();
            $table->rememberToken();
            $table->timestamps(); 
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
