<?php

namespace App\Exports;

use App\Models\User;
use DB;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class AllUsersExport implements FromCollection, WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        //return User::all();

        DB::statement(DB::raw('set @row=0'));
        $data = User::selectRaw('@row:=@row+1 as srno, name, email, DATE_FORMAT(dob, "%d %M %Y")')->get();
        return $data;

    }

    public function headings(): array
    {
        return [
            'Sr.No',
            'Name',
            'Email',
            'Birthdate'
        ];
    }
}
