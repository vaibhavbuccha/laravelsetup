<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Notifications\AdminResetPassword;
use Auth;

class Admin extends Authenticatable
{
    use HasFactory;
    use Notifiable;

    public static function getAllAuthUser(){
		$userId = Auth::guard('admin')->user()->id;
		$results = Admin::find($userId);
		return $results;
	}
	
    public function sendPasswordResetNotification($token) {
		$this->notify(new AdminResetPassword($token));
	}
}
