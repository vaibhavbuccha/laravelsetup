<?php

namespace App\Helper;
use App\User;
use Mail;
use Config;
use Stripe;
use DB;

class StripeHelper
{
    /**
       * Set stripe api key
    */
    public static function setAPIKey(){
        Stripe\Stripe::setApiKey(Config::get('constant.STRIPE_SECRET_KEY'));
    }

    /**
       * Create stripe customer account
    */
    public static function createStripeAccount($dataArray = []) {
        Self::setAPIKey();
        try {
            $stripeCustomers = \Stripe\Customer::create([
                        "email" => $dataArray['email'],
                        "description" => "User: " . $dataArray['userId'],
            ]);
            $accountId = $stripeCustomers['id'];

            return array("status" => true, "accountId" => $accountId);
        } catch (Exception $e) {
            return array("status" => false, "stripeError" => ucfirst($e->getMessage()));
        }
    }

    /**
       * Create stripe card token 
    */
    public static function generateStripeToken($cardNumber, $expMonth, $expYear, $cvc) {  
       
       Self::setAPIKey();
       
       $stripeToken = \Stripe\Token::create([
               "card" => [
                 "number" => trim($cardNumber),
                 "exp_month" => $expMonth,
                 "exp_year" => $expYear,
                 "cvc" => $cvc
               ]
             ]);
       
       return $stripeToken['id'];
    }

    /**
       * Save user card
    */
    public static function saveCard($dataArray = []) {
        
        Self::setAPIKey();

        try {
            $stripeCardToken = \Stripe\Token::create([
                'card' => [
                    'number' => trim($dataArray['cardNumber']),
                    'exp_month' => $dataArray['cardExpMonth'],
                    'exp_year' => $dataArray['cardExpYear'],
                    'cvc' => $dataArray['cardCvv'],
                    'address_line1' => $dataArray['addressLine1'],
                    'address_line2' => $dataArray['addressLine2'],
                    'address_state' => $dataArray['state'],
                    'address_city' => $dataArray['city'],
                    'address_country' => $dataArray['country'],
                    'address_zip' => $dataArray['zip'],
                    'name' => $dataArray['nameOnCard']

                ]
            ]);
            
            $cardToken = $stripeCardToken['id'];
            //echo $dataArray['stripeCustomerId']; die;
            $customer = \Stripe\Customer::retrieve($dataArray['stripeCustomerId']);
            $stripeCard = $customer->sources->create(
                [
                  'source' => $cardToken,
                ]
            );
            
            if(isset($dataArray['isDefault']) && $dataArray['isDefault'] == 1){
                
                \Stripe\Customer::update($dataArray['stripeCustomerId'], [
                    'default_source' => $stripeCard['id'],
                ]);
            }
            
            return array("status" => true, "cardId" => $stripeCard['id']);
        } catch (Exception $e) {
            return array("status" => false, "stripeError" => ucfirst($e->getMessage()));
        }
    }

    /**
       * Get user card details
    */
    public static function getCardDetails($customerId, $cardId) {
        Self::setAPIKey();
        try {
            $customer = \Stripe\Customer::retrieve($customerId);
            $card = $customer->sources->retrieve($cardId);
            $responseArray = [];
            $responseArray['cardNumber'] = $card['last4'];
            $responseArray['brand'] = $card['brand'];
            $responseArray['expMonth'] = $card['exp_month'];
            $responseArray['expYear'] = $card['exp_year'];
            $responseArray['funding'] = $card['funding'];
            //echo $stripeCard['id']; die;
            return array("status" => true, "cardDetails" => $responseArray);
        } catch (Exception $e) {
            //echo $e->getMessage(); die;
            return array("status" => false, "stripeError" => ucfirst($e->getMessage()));
        }
    }

    /**
       * Make as default card
    */
    public static function setDefaultCard($customerId, $cardId) {
        Self::setAPIKey();
        try {

            $response = \Stripe\Customer::update($customerId, [
                'default_source' => $cardId,
            ]);

            return array("status" => true, "data" => $response);
        } catch (Exception $e) {
            //echo $e->getMessage(); die;
            return array("status" => false, "stripeError" => ucfirst($e->getMessage()));
        }
    }

    /**
       * Delete user card
    */
    public static function removeCardDetails($customerId, $cardId) {
        Self::setAPIKey();
        try {

            $customer = \Stripe\Customer::retrieve($customerId);
            $response = $customer->sources->retrieve($cardId)->delete();

            return array("status" => true, "data" => $response);
        } catch (Exception $e) {
            //echo $e->getMessage(); die;
            return array("status" => false, "stripeError" => ucfirst($e->getMessage()));
        }
    }

    /**
       * Make payment from users card
    */
    public static function createCharge($requestArray = []){
        Self::setAPIKey();
        try {
            $stripeParentCharge = \Stripe\Charge::create([
                "amount" => $requestArray['amount'] * 100,
                "currency" => Config::get('constant.APP_CURRENCY'),
                "customer" => $requestArray['customerId'],
                "source" => $requestArray['cardId'], 
                "description" => $requestArray['description'],
                //"capture" => true
            ]);

            $transactionId = $stripeParentCharge['id'];
           
            //echo $stripeCard['id']; die;
            return array("status" => true, "transactionId" => $transactionId, "chargeData" => $stripeParentCharge);
        } 
        catch (Stripe\Exception\CardException $e) {
            $errorObj = $e->getError();
            $transactionId = $errorObj->charge;
            return array("status" => false, "transactionId" => $transactionId, "stripeError" => ucfirst($e->getMessage()), "chargeData" => $errorObj);
        }
        catch (Exception $e) {
            //echo $e->getMessage(); die;
            return array("status" => false, "stripeError" => ucfirst($e->getMessage()));
        }
    }

    /**
       * Getting all card
    */
    public static function getAllCard($customerId) {
        Self::setAPIKey();
        try {
            $customer = \Stripe\Customer::retrieve($customerId);
            $card = $customer->sources->all();

            return array("status" => true, "cardDetails" => $card);
        } catch (Exception $e) {
            //echo $e->getMessage(); die;
            return array("status" => false, "stripeError" => ucfirst($e->getMessage()));
        }
    }

    /**
       * Refund charge
    */
    public static function refundCharge($transactionId, $amount = 0) {
        try {
            Self::setAPIKey();
            $charge = \Stripe\Refund::create(array(
                        "charge" => $transactionId,
                        "amount" => round($amount,2)*100,
            ));
            return array("status" => true, "chargeData" => $charge);
        } catch (Stripe_CardError $e) {
            return array("message" => "Error", "status" => false, "stripeError" => ucfirst($e->getMessage()));
        } catch (Stripe_InvalidRequestError $e) {
            return array("message" => "Error", "status" => false, "stripeError" => ucfirst($e->getMessage()));
        } catch (Stripe_AuthenticationError $e) {
            return array("message" => "Error", "status" => false, "stripeError" => ucfirst($e->getMessage()));
        } catch (Stripe_ApiConnectionError $e) {
            return array("message" => "Error", "status" => false, "stripeError" => ucfirst($e->getMessage()));
        } catch (Stripe_Error $e) {
            return array("message" => "Error", "status" => false, "stripeError" => ucfirst($e->getMessage()));
        } catch (Exception $e) {
            return array("message" => "Error", "status" => false, "stripeError" => ucfirst($e->getMessage()));
        }
    }

    /**
       * Create bank account token
    */
    public static function bankAccountToken($requestArray) {
        try {
            Self::setAPIKey();
            
            $bankAccountToken = \Stripe\Token::create([
                "bank_account" => [
                  "country" => $requestArray['country'],
                  "currency" => $requestArray['currency'],
                  "account_holder_name" => $requestArray['accountHolderName'],
                  "account_holder_type" => strtolower($requestArray['accountHolderType']),
                  "routing_number" => $requestArray['routingNumber'],
                  "account_number" => $requestArray['accountNumber'],
                  //"usage" => "source"
                ]
            ]);

            // $bankToken = $bankAccountToken;
            $bankToken = $bankAccountToken['id'];
            return array("status" => true, "bankAccountToken" => $bankToken, "responseObj" => $bankAccountToken);
        } catch (Exception $e)
        {
            return array("message" => "Error", "status" => false, "stripeError" => ucfirst($e->getMessage()));
        }
    }

    /**
       * Create extrnal account
    */
    public static function createAccount($requestArray) {
        try {
            Self::setAPIKey();
            
            $createStripeAccount = \Stripe\Account::create([
                "type" => "custom",
                "country" => $requestArray['country'],
                "business_type" => strtolower($requestArray['accountHolderType']),
                "email" => $requestArray['email'],
                //"requested_capabilities" => ["legacy_payments"],
                'requested_capabilities' => ['transfers'],
                'individual' => array(
                    'dob' => array(
                        'day' => date("d", strtotime($requestArray['dob'])),
                        'month' => date("m", strtotime($requestArray['dob'])),
                        'year' => date("Y", strtotime($requestArray['dob']))
                    ),
                    'address' => array(
                        'country' => Config::get('constant.APP_COUNTRY'),
                        'line1' => "address_full_match", // testing
                        /*'line1' => $requestArray['address'],*/ // Live
                        'line2' => $requestArray['address'], //$requestArray['city'],
                        'city' => $requestArray['city'],
                        'state' => $requestArray['state'],
                        'postal_code' => $requestArray['postalcode'],
                    ),
                    // 'verification' => array(
                    //     'document' => array(
                    //         "front" => $requestArray['frontDocument'],
                    //         "back" => $requestArray['backDocument']
                    //     ),      
                    // ),
                    'first_name' => $requestArray['firstname'],
                    'last_name' => $requestArray['lastname'],
                    //'ssn_last_4' => $requestArray['ssnNumber'],
                    'email' => $requestArray['email'],
                    'phone' => "+61".$requestArray['phone'],
                ),
                'business_profile' => ['mcc' => '4722','url'=>"https://google.com"],
                'tos_acceptance' => array(
                    'date' => time(),
                    'ip' => $_SERVER['REMOTE_ADDR']
                ),
            ]);

            $stripeAccount = $createStripeAccount['id'];
            return array("status" => true, "stripeAccountId" => $stripeAccount);
            //return $createStripeAccount;
        } catch (Exception $e)
        {
            return array("message" => "Error", "status" => false, "stripeError" => ucfirst($e->getMessage()));
        }
    }

    /**
       * Create bank account
    */
    public static function createBankAccount($accountId, $bankToken) {
        try {
            Self::setAPIKey();
            
            $account = \Stripe\Account::retrieve($accountId);

            $bank_detail = $account->external_accounts->create(["external_account" => $bankToken, "default_for_currency" => true]);

            $bankPaymentId = $bank_detail['id']; 
            return array("status" => true, "bankPaymentId" => $bankPaymentId, "bankAccountObj" => $bank_detail);
        } catch (Exception $e)
        {
            return array("message" => "Error", "status" => false, "stripeError" => ucfirst($e->getMessage()));
        }
    }
    
    /**
       * Upload stripe document
    */
    public static function uploadStripeDocument($documentName){
        Self::setAPIKey();

        $fileObjBack = \Stripe\FileUpload::create(
            [
              "purpose" => "identity_document",
              "file" => fopen('./uploads/stripeDocuments/'.$documentName, 'r')
            ]
        );
        $fileDataBack = json_decode(json_encode($fileObjBack), true);
        return $fileDataBack['id'];
    }

    /**
       * Uplaod image in server
    */
    public static function imageUpload($fileName){
        
        $image_array_1 = explode(";", $fileName);

        $image_array_2 = explode(",", $image_array_1[1]);

        $fileName = base64_decode($image_array_2[1]);

        $imageName =  time()."-".uniqid().'.png';
        $uploadImage = "uploads/stripeDocuments/".$imageName;

        file_put_contents($uploadImage, $fileName);

        return $imageName;
    }

    /**
       * Get users all bank accounts
    */
    public static function getAllBank($accountId){
        try{
            Self::setAPIKey();

            $account = \Stripe\Account::retrieve($accountId);
            return array("status" => true, "bankAccount" => $account['external_accounts']['data'], "payoutStatus" => $account['payouts_enabled'], "currentlyDue"=> $account['requirements']['currently_due'], "documentStatus" => $account['individual']['verification']['status']);

        } catch (Exception $e)
        {
            return array("message" => "Error", "status" => false, "stripeError" => ucfirst($e->getMessage()));
        }
    }

    /**
       * Update users specific bank account
    */
    public static function updateBankAccount($accountId, $bankAccountId){
        try{
            Self::setAPIKey();

            $ch = curl_init();

            curl_setopt($ch, CURLOPT_URL, 'https://api.stripe.com/v1/accounts/'.$accountId.'/external_accounts/'.$bankAccountId);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, "default_for_currency=true");
            curl_setopt($ch, CURLOPT_USERPWD, Config::get('constant.STRIPE_SECRET_KEY'));

            $headers = array();
            $headers[] = 'Content-Type: application/x-www-form-urlencoded';
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

            $result = curl_exec($ch);
            if (curl_errno($ch)) {
                echo 'Error:' . curl_error($ch);
            }
            curl_close($ch);

            return array("status" => true);

        } catch (Exception $e)
        {
            return array("message" => "Error", "status" => false, "stripeError" => ucfirst($e->getMessage()));
        }
    }

    /**
       * Transfer money to bank account
    */
    public static function createTransfer($requestArray = []){
        Self::setAPIKey();
        try {
            $stripeParentCharge = \Stripe\Transfer::create([
                "amount" => $requestArray['amount'] * 100,
                "currency" => Config::get('constant.APP_CURRENCY'),
                "destination" => $requestArray['customerBankId'],
                "transfer_group" => $requestArray['description']
            ]);

            $transactionId = $stripeParentCharge['id'];
            
            return array("status" => true, "transactionId" => $transactionId, "responseData" => $stripeParentCharge);
        } catch (Exception $e) {
            //echo $e->getMessage(); die;
            return array("status" => false, "stripeError" => ucfirst($e->getMessage()));
        }
    }

    /**
       * Delete bank account
    */
    public static function deleteBankAccount($accountId, $bankAccountId){
        try{
            $ch = curl_init();

            curl_setopt($ch, CURLOPT_URL, 'https://api.stripe.com/v1/accounts/'.$accountId.'/external_accounts/'.$bankAccountId);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'DELETE');

            curl_setopt($ch, CURLOPT_USERPWD, STRIPE_SECRET_KEY);

            $result = curl_exec($ch);
            if (curl_errno($ch)) {
                echo 'Error:' . curl_error($ch);
            }
            curl_close($ch);

            return array("status" => true);

        } catch (Exception $e)
        {
            return array("message" => "Error", "status" => false, "stripeError" => ucfirst($e->getMessage()));
        }
    }
}
