<?php

namespace App\Helper; 
use App\User;
use App\Models\NotificationMessage;
use App\Models\ApplyCollab;
use App\Models\DrawList;
use App\Models\NewLocation;
use App\Models\NewLocationLevel1;
use App\Models\NewLocationLevel2;
use App\Models\AgencyInfluencer;
use App\Models\CancelPaymentDispute;
use App\Models\Country;
use App\Models\UserSocialmedia;
use App\Models\BlockedUser;
use App\Models\City;
use Mail;
use Config;
use Image;
use Carbon\Carbon;
use App\Helper\StripeHelper;
use App\Models\WalletBalance;
use App\Models\WalletTransaction;
use App\Models\Rating;
use App\Models\PaymentReceipt;
use App\Models\CardTransaction;
use App\Models\Plan;
use App\Models\Subscription;
use Auth;
use App\Notifications\JoinedViaReferral;
use App\Notifications\Disputes\DisaputeAutoResolvedInfluencer;
use App\Notifications\Disputes\DisaputeAutoResolvedBrand;
use App\Notifications\Disputes\DisputeEscalateAutoCompleteInfluencerReceivedAmount;
use App\Notifications\Disputes\DisputeEscalateAutoCompleteBrandNotRefund;
use App\Notifications\Disputes\DisaputeAutoResolvedInfluencerCancelPayment;
use App\Notifications\Disputes\DisaputeAutoResolvedBrandCancelPayment;
use App\Notifications\Disputes\DisputeEscalateAutoCompleteInfluencerReceivedAmountCancelPayment;
use App\Notifications\Disputes\DisputeEscalateAutoCompleteBrandNotRefundCancelPayment;
use App\Helper\FirebaseHelper;
use Illuminate\Support\Facades\Log;
use App\Notifications\Invoice\influencerInvoice;
use App\Notifications\Invoice\brandInvoice;
use App\Notifications\CompleteCollaborationBrand;
use App\Notifications\CompleteCollaborationInfluencer;

//use Stripe;

class Helper {
    
    /**
       * Sending API response
    */
    public static function ApiResponse($responseArray){
        return response()->json($responseArray);
    }

    /**
       * Set date format
    */
    public static function dateFormat($value)
    {
        return \Carbon\Carbon::parse($value)->format('Y-m-d');
    }

    /**
       * Check specific user access token 
    */
    public static function checkAccessToken($userId, $accessToken){
        try{
            $user = User::where("id",$userId)->where("access_token",$accessToken)->where('role',0)->first();
            if($user){ 
                if ($user->is_active == 0) {
                    $user->access_token = null;
                    $user->save();
                    return ['status' => 0, 'message' => Helper::getNotificationMessage('account_inactive'),"data" => $user,'responseStatus' => 3];
                }
                else{
                    return ['status' => 1,'message' => "Login successfully.","data" => $user,'responseStatus' => 1];
                }
            } 
            else{
                $user = User::where("id",$userId)->first();
                if($user){
                    if ($user->is_active == 0) {
                        $user->access_token = null;
                        $user->save();
                        return ['status' => 0, 'message' =>  Helper::getNotificationMessage('account_inactive'),"data" => $user,'responseStatus' => 3];
                    }
                    else{
                        return ['status' => 0, 'message' => Helper::getNotificationMessage('access_token_expire'),'responseStatus' => 2];
                    }
                }
                else{
                    return ['status' => 0, 'message' => Helper::getNotificationMessage('access_token_expire'),'responseStatus' => 2];
                }
            }
        }
        catch (\Exception $e) {
            return ['status' => 0, 'message' => $e->getMessage()];
        }
    }

    /**
       * Send custom push notification to device
    */
    public static function sendNotificationToDevice($deviceToken,$notification_type = 0,$message, $notificationDatas = []){
        //FCM api URL
        $url = 'https://fcm.googleapis.com/fcm/send';
        //api_key available in Firebase Console -> Project Settings -> CLOUD MESSAGING -> Server key
        $server_key = "AAAAiTNbq1M:APA91bFDouDwD1RzPyryMZoqF_5LGCyNHTLTmYF_5-_a7SaCkdNsR2XZfmL4W1dfdx2lcAioHKi0beJIJR6DU5OlEny5yJImjy45gldWLiyt5OIHTzlssPEpEonOZMC1AaqNvauZda4v";
        $fields = array();
        
        if (is_array($deviceToken)) {
            $fields['registration_ids'] = $deviceToken;
        } else {
            
            $fields['to'] = $deviceToken;
        }

        $fields['data'] = array(
            "message" => $message,
            'sound'=>'Default',
            'notification_type' => $notification_type,
            'user_message' => isset($notificationDatas['user_message']) ? $notificationDatas['user_message'] : "",
        );
        
        $fields['notification'] = array(
            "body" => $message,
            'sound'=>'Default',
            'notification_type' => $notification_type,
            'user_message' => isset($notificationDatas['user_message']) ? $notificationDatas['user_message'] : "",
        );
        //print_r($fields); die;
        //header with content_type api key
        $headers = array(
            'Content-Type:application/json',
            'Authorization:key=' . $server_key,
        );

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        $result = curl_exec($ch);
        //print_r($result);die;
        if ($result === FALSE) {
            die('FCM Send Error: ' . curl_error($ch));
        }
        curl_close($ch);
    }

    /**
       * Get notification message by access key
    */
    public static function getNotificationMessage($access_key){
        try{
            $notificationData = NotificationMessage::select('message')->where("access_key",$access_key)->first();
            if($notificationData){
                return $notificationData->message;
            }
            else{
                return "Message not found with key: ".$access_key;
            }
        }
        catch (\Exception $e) {
            return ['status' => 0, 'message' => $e->getMessage()];
        }
    }
    
    /**
       * Send custom email
    */
    public static function sendMail($post = null, $templateName = null) {
        if ($templateName) {
            $path = 'emails/' . $templateName;
        } else {
            $path = 'emails/defaultEmail';
        }
        try{
            Mail::send($path, ['post' => $post], function($message) use ($post) {
                $message->from(Config::get('constant.FROM_EMAIL'), Config::get('constant.REPLAY_NAME'));
                $message->to($post['email'])->subject($post['subject']);
                if(isset($post['attachment']) && $post['attachment']){
                    $message->attach($post['attachment']);
                }
            });
            return ['status' => 1, 'message' => "Email sent successfully."];
        }
        catch (\Exception $e) {
            //dd($e->getMessage());
            return ['status' => 0, 'message' => $e->getMessage()];
        }
    }
    
    /**
       * Send admin email
    */
    public static function sendAdminMail($post = null, $templateName = null) {
        if ($templateName) {
            $path = 'emails/' . $templateName;
        } else {
            $path = 'emails/defaultEmail';
        }
       
        try{
            Mail::send($path, ['post' => $post], function($message) use ($post) {
                $message->from($post['email']);
                $message->to(Config::get('constant.ADMIN_EMAIL'), Config::get('constant.ADMIN_NAME'))->subject($post['subject']);
                if(isset($post['attachment']) && $post['attachment']){
                    $message->attach($post['attachment']);
                }
            });

            return ['status' => 1, 'message' => "Email sent successfully."];
        }
        catch (\Exception $e) {
            return ['status' => 0, 'message' => $e->getMessage()];
        }
    }

    /**
       * Generate stripe token
    */
    public static function generateStripeToken($cardNumber, $expMonth, $expYear, $cvc) {  
       
       Stripe\Stripe::setApiKey(Config::get('constant.STRIPE_SECRET'));
       
       $stripeToken = \Stripe\Token::create([
               "card" => [
                 "number" => trim($cardNumber),
                 "exp_month" => $expMonth,
                 "exp_year" => $expYear,
                 "cvc" => $cvc
               ]
             ]);
       
       return $stripeToken['id'];
   }

   /**
       * Image resize
    */
   public static function ImageResize($finalWidth,$image,$destinationPath,$unlinkImagePath = null,$indexVal = 0){
        $finalWidth = 250;
        $imageMainPath = 'uploads/'.$destinationPath;
        $imageThumbPath = '/uploads/'.$destinationPath.'/thumbnail/';

        if (!is_dir(public_path($imageMainPath))) {
            mkdir(public_path($imageMainPath), 777, true);         
        }
        if (!is_dir(public_path($imageThumbPath))) {
            mkdir(public_path($imageThumbPath), 777, true);         
        }


        if($unlinkImagePath){
            $filename = public_path($imageMainPath.'/'.$unlinkImagePath);
            if(file_exists($filename)){
                unlink($filename);
            }
            $filenameThumb = public_path($imageThumbPath.$unlinkImagePath);
            if(file_exists($filenameThumb)){
                unlink($filenameThumb);
            }
        }

        /*$imageName = time().'-WordTribe-'.$indexVal.'.jpg';
        $destinationPath = public_path($imageMainPath);
        if($multiple){
            move_uploaded_file($_FILES[$fileKeyName]['tmp_name'][$indexVal],$destinationPath."/".$imageName);
        }
        else{
            move_uploaded_file($_FILES[$fileKeyName]['tmp_name'],$destinationPath."/".$imageName);
        }*/
        
       /* $width  = 768;
        $height = 1024;*/

        list($width, $height) = getimagesize($image);
        $newHeight = floor($height * ($finalWidth / $width));

        $imageName = time().'-adminlte-'.$indexVal.'.jpg';
        $destinationPath = public_path($imageMainPath);
        $image_resize = Image::make($image->getRealPath());
        $image->move($destinationPath,$imageName);                
        $image_resize->resize($finalWidth,$newHeight);
        $image_resize->save(public_path($imageThumbPath.$imageName));

        return $imageName;
    }

    /**
       * Store thumbnail image
    */
    public static function storeThumbnail($image,$destinationPath,$finalWidth=100){
        $source_image = imagecreatefrompng($image);
        list($width,$height) = getimagesize($image);
        $newwidth = 400;
        $newheight = 400;
        $thumb = imagecreatetruecolor(400,400);
        imagecopyresampled($thumb, $source_image, 0, 0, 0, 0, $newwidth, $newheight, $width, $height);        
        imagegif($thumb, $destinationPath);
        return true;
    }

    /**
       * Uplaod image
    */
    public static function imageUpload($finalWidth,$image,$destinationPath,$unlinkImagePath = null,$fileKeyName,$indexVal = 0,$multiple = false){

        $imageMainPath = 'uploads/'.$destinationPath;
        $imageThumbPath = '/uploads/'.$destinationPath.'/thumbnail/';
        if (!is_dir(public_path($imageMainPath))) {
            mkdir(public_path($imageMainPath), 777, true);         
        }
        if (!is_dir(public_path($imageThumbPath))) {
            mkdir(public_path($imageThumbPath), 777, true);         
        }

        if($unlinkImagePath){
            $filename = public_path($imageMainPath.'/'.$unlinkImagePath);
            if(file_exists($filename)){
                unlink($filename);
            }
            $filenameThumb = public_path($imageThumbPath.$unlinkImagePath);
            if(file_exists($filenameThumb)){
                unlink($filenameThumb);
            }
        }
        
        $imageName = time().'-adminlte-'.$indexVal.'.jpg';
        $destinationPath = public_path($imageMainPath);
        if($multiple){
            move_uploaded_file($_FILES[$fileKeyName]['tmp_name'][$indexVal],$destinationPath."/".$imageName);
        }
        else{
            move_uploaded_file($_FILES[$fileKeyName]['tmp_name'],$destinationPath."/".$imageName);
        }
        return $imageName;
    }

    /**
       * Upload image with resize
    */
    public static function ImageUploadResize($finalWidth,$image,$destinationPath,$unlinkImagePath = null,$fileKeyName,$indexVal = 0,$multiple = true){

        $imageMainPath = 'uploads/'.$destinationPath;
        $imageThumbPath = '/uploads/'.$destinationPath.'/thumbnail/';

        if (!is_dir(public_path($imageMainPath))) {
            mkdir(public_path($imageMainPath), 777, true);         
        }
        if (!is_dir(public_path($imageThumbPath))) {
            mkdir(public_path($imageThumbPath), 777, true);         
        }


        if($unlinkImagePath){
            $filename = public_path($imageMainPath.'/'.$unlinkImagePath);
            if(file_exists($filename)){
                unlink($filename);
            }
            $filenameThumb = public_path($imageThumbPath.$unlinkImagePath);
            if(file_exists($filenameThumb)){
                unlink($filenameThumb);
            }
        }

        $finalHeight = 140;
        list($width, $height) = getimagesize($image);
        $newwidth = floor($width * ($finalHeight / $height));

         $imageName = time().'-adminlte-'.$indexVal.'.jpg';
        $destinationPath = public_path($imageMainPath);
        $image_resize = Image::make($image->getRealPath());
        $image->move($destinationPath,$imageName);                
        $image_resize->resize($newwidth,$finalHeight);
        
        //$image_resize->resize(74,140);
        $image_resize->save(public_path($imageThumbPath.$imageName));

        return $imageName;
    }

    /**
       * Link find and conver to url
    */
    public static function findlinkandconvert($text)
    {
        $text = " ".$text." ";
        return nl2br(trim(preg_replace_callback('/((http[s]?:\/\/)?(?>[a-z\-0-9]{2,}\.){1,}[a-z]{2,8})(?:\s|\/)/m', function($match)
        {

            $text   = trim($match[0]);
            $pieces = parse_url($text);
            $scheme = array_key_exists('scheme', $pieces) ? $pieces['scheme'] : 'http';
            $host   = isset($pieces['host']) ? $pieces['host'] : $pieces['path'];
            $link   = sprintf('%s://%s', $scheme, $host);
            return sprintf('<a class="pink-color-text" href="%s" target="_blank">%s</a> ', $link, $text);
        }, $text)));
    }

    /**
       * Draw list
    */
    public static function drawList($influenceId){
        try{
            $drawCount = ApplyCollab::where('user_id' , $influenceId)->get()->count();
            $influencer = User::find($influenceId);
            if($drawCount == 3){
                if (DrawList::where('influencer_id' , $influenceId)->get()->count() != 1) {
                    if ($influencer->country == 13) {
                        $draw = new DrawList();
                        $draw->influencer_id = $influenceId;
                        $draw->save();
                    }
                }
            }
        }
        catch (\Exception $e) {
            return ['status' => 0, 'message' => $e->getMessage()];
        }
    }
    public static function ParseUlrHelper($url)
    {
        $urlStr = '';
        if (!empty($url)) {
            $parsed = parse_url($url);
            if (empty($parsed['scheme'])) {
                $urlStr = 'http://' . ltrim($url, '/');
            }else {
                $urlStr = $url;
            }
        }
        return $urlStr;
    }

    /**
       * Filtering and Store location data 
    */
    public static function storeLocationData($location_data,$country_code,$countryId = FALSE)
    {
        if (empty($location_data)) {
            return array();
        }
        $location_data = json_decode($location_data,true);
        if ($countryId) {
        $country_id = $country_code;
        }else{
        $country = Country::where('country_code',$country_code)->get()->first();
        $country_id = $country->id;
        }
        $is_suburb = 0;
        $newLocation = false;
        $administrative_area_level_2 = false;
        $administrative_area_level_1 = false;
        if (!empty($location_data['address_components'])) {
            $address_components = $location_data['address_components'];
            // foreach ($address_components as $address_component) {

            if (isset($address_components[2]) && in_array('administrative_area_level_1', $address_components[2]['types'])) {

                $administrative_area_level_1 = NewLocationLevel1::updateOrCreate([
                    'name'       => $address_components[2]['short_name'],
                    'country_id' => $country_id,
                ],[
                    'long_name' => $address_components[2]['long_name'],
                ]);

                /* Start Get City name */
                    //dd($location_data['place_id']);
                    $cityName = "";
                    /*print_r($location_data['geometry']['location']);
        die;*/
                    if(isset($location_data['geometry']) && isset($location_data['geometry']['location'])){
                        $lat = isset($location_data['geometry']['location']['lat']) ? $location_data['geometry']['location']['lat'] : 0;
                        $long = isset($location_data['geometry']['location']['lng']) ? $location_data['geometry']['location']['lng'] : 0;
                        if($long != 0 && $lat != 0){
                            $curl = curl_init();
                            curl_setopt_array($curl, array(
                              CURLOPT_URL => 'https://maps.googleapis.com/maps/api/geocode/json?latlng='.$lat.','.$long.'&key='.Config::get('constant.GOOGLE_MAP_API_KEY'),
                              CURLOPT_RETURNTRANSFER => true,
                              CURLOPT_ENCODING => '',
                              CURLOPT_MAXREDIRS => 10,
                              CURLOPT_TIMEOUT => 0,
                              CURLOPT_FOLLOWLOCATION => true,
                              CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                              CURLOPT_CUSTOMREQUEST => 'GET',
                            ));

                            $responseCity = curl_exec($curl);
                            $responseCity = json_decode($responseCity, true);
                            curl_close($curl);
                            //dd($responseCity['results']);
                            foreach($responseCity['results'] as $cityLocation){
                                $typesArr = isset($cityLocation['address_components'][0]['types']) ? $cityLocation['address_components'][0]['types'] : array();

                                if(isset($cityLocation['address_components'][0]['types']) && in_array("colloquial_area", $cityLocation['address_components'][0]['types'])){
                                    
                                    $cityName = isset($cityLocation['address_components'][0]['short_name']) ? $cityLocation['address_components'][0]['short_name'] : "";

                                    if($cityName){
                                        $administrative_area_level_2 = NewLocationLevel2::updateOrCreate([
                                            'name'       => $cityName,
                                            'country_id' => $country_id,
                                            'level_1_id' => $administrative_area_level_1->id,
                                        ],[
                                            'long_name' => $cityLocation['address_components'][0]['long_name'],
                                        ]);
                                    }
                                }
                            }
                        }
                        
                    }
                    
                /* End Get City name */

                if(empty($cityName)){
                    $administrative_area_level_2 = NewLocationLevel2::updateOrCreate([
                        'name'       => $address_components[1]['short_name'],
                        'country_id' => $country_id,
                        'level_1_id' => $administrative_area_level_1->id,
                    ],[
                        'long_name' => $address_components[1]['long_name'],
                    ]);
                }
                
                $newLocation = NewLocation::updateOrCreate([
                    'name'       => $address_components[0]['short_name'],
                    'country_id' => $country_id,
                    'level_2_id' => $administrative_area_level_2->id,
                ],[
                    'long_name' => $address_components[0]['long_name'],
                ]);
                $is_suburb = 1;
            } elseif (isset($address_components[1])) {
                if (in_array('administrative_area_level_1', $address_components[1]['types'])) {
                     $administrative_area_level_1 = NewLocationLevel1::updateOrCreate([
                        'name'       => $address_components[1]['short_name'],
                        'country_id' => $country_id,
                    ],[
                        'long_name' => $address_components[1]['long_name'],
                    ]);
                    $administrative_area_level_2 = NewLocationLevel2::updateOrCreate([
                        'name'       => $address_components[0]['short_name'],
                        'country_id' => $country_id,
                        'level_1_id' => $administrative_area_level_1->id,
                    ],[
                        'long_name' => $address_components[0]['long_name'],
                    ]);
                    $newLocation = NewLocation::updateOrCreate([
                        'name' => $address_components[0]['short_name'],
                        'long_name'  => $address_components[0]['long_name'],
                        'country_id' => $country_id,
                        'level_2_id' => $administrative_area_level_1->id,
                    ]);
                } elseif (in_array('administrative_area_level_2', $address_components[1]['types'])) {
                    $administrative_area_level_1 = NewLocationLevel1::updateOrCreate([
                        'name'       => $address_components[2]['short_name'],
                        'country_id' => $country_id,
                    ],[
                        'long_name' => $address_components[2]['long_name'],
                    ]);
                    $administrative_area_level_2 = NewLocationLevel2::updateOrCreate([
                        'name'       => $address_components[1]['short_name'],
                        'country_id' => $country_id,
                        'level_1_id' => $administrative_area_level_1->id,
                    ],[
                        'long_name' => $address_components[1]['long_name'],
                    ]);
                    $newLocation = NewLocation::updateOrCreate([
                        'name'       => $address_components[0]['short_name'],
                        'country_id' => $country_id,
                        'level_2_id' => $administrative_area_level_2->id,
                    ],[
                        'long_name' => $address_components[0]['long_name'],
                    ]);
                    $is_suburb = 1;
                }
                if (!$newLocation) {
                    $administrative_area_level_1 = NewLocationLevel1::updateOrCreate([
                        'name'       => $address_components[0]['short_name'],
                        'country_id' => $country_id,
                    ],[
                        'long_name' => $address_components[0]['long_name'],
                    ]);
                    $administrative_area_level_2 = NewLocationLevel2::updateOrCreate([
                        'name'       => $address_components[0]['short_name'],
                        'country_id' => $country_id,
                        'level_1_id' => $administrative_area_level_1->id,
                    ],[
                        'long_name' => $address_components[0]['long_name'],
                    ]);
                    $newLocation = NewLocation::updateOrCreate([
                        'name'       => $address_components[0]['short_name'],
                        'country_id' => $country_id,
                        'level_2_id' => $administrative_area_level_1->id,
                    ],[
                        'long_name' => $address_components[0]['long_name'],
                    ]);
                }

            }
            $city = City::updateOrCreate([
                'name'       => $administrative_area_level_2->name,
                'country_id' => $country_id,
                'level_2_id' => $administrative_area_level_2->id,
                'state_id'   => $administrative_area_level_1->id,
            ],[
                'is_suburb' => 0,
            ]);
            $city = City::updateOrCreate([
                'name'       => $newLocation->name,
                'country_id' => $country_id,
                'level_2_id' => $administrative_area_level_2->id,
                'state_id'   => $administrative_area_level_1->id,
            ],[
                'is_suburb' => $is_suburb
            ]);

            return array(
                'city_id'    => $city->id,
                'country_id' => $country_id
            );
        }
    }

    /**
       * Get active influencer
    */
    public static function getActiveInfluencer($agency_id)
    {
        $influencer = AgencyInfluencer::where('agency_id',$agency_id)->where('is_active','=',1)->first();
        if (empty($influencer)) {
            return false;
        } else {
            return $influencer->influencer;
        }
    }

    /**
       * Send verify email
    */
    public static function sendVerifyEmail($role,$userId,$email)
    {
        $post = [];
        if ($role == 1) {
            $post['email'] = $email;
            $post['userid'] = $userId;
            $post['subject'] = "Welcome to ".Config::get('constant.APP_SIMPLE_NAME');
            $template = 'influencerRegisterEmail';
        } elseif ($role == 2) {
            $post['email'] = $email;
            $post['userid'] = $userId;
            $post['subject'] = "Welcome to ".Config::get('constant.APP_SIMPLE_NAME');
            $template = 'businessRegisterEmail';
        } elseif ($role == 3) {
            $post['email'] = $email;
            $post['userid'] = $userId;
            $post['subject'] = "Welcome to ".Config::get('constant.APP_SIMPLE_NAME');
            $template = 'agencyRegisterEmail';
        }
        if (!empty($post)) {
            $result = self::sendMail($post,$template);
            return $result;
        }else{
            return false;
        }
       
    }

    /**
       * Money transfer from wallet to bank
    */
    public static function moneyTransferredFromWalletToBank($amount, $userId, $accountId = 0){
        $userDB = User::find($userId);
        if($userDB){
            if($userDB->wallet){
                if($userDB->wallet->balance >= $amount){
                    if($accountId){
                        $userBankAccount = $userDB->bankAccounts->where("id",$accountId)->first();
                    }
                    else{
                        $userBankAccount = $userDB->bankAccounts->where("is_default",'1')->first();
                    }
                    if($userBankAccount){
                        try{
                            $adminCharge = ($amount * Config::get('constant.ADMIN_CHARGE')) / 100;

                            $transferArr = [];
                            $transferArr['amount'] = ($amount - $adminCharge);
                            $transferArr['customerBankId'] = $userDB->stripe_account_id;
                            $transferArr['description'] = "Transfer by user from wallet";

                            $transferredTransaction = StripeHelper::createTransfer($transferArr);

                            if($transferredTransaction['status'] == true){
                                $remainingBalance = $userDB->wallet->balance - $amount;

                                $userDB->wallet->balance = $remainingBalance;
                                $userDB->wallet->save();

                                $walletTransactionDB = new WalletTransaction();
                                $walletTransactionDB->user_id = $userId;
                                $walletTransactionDB->card_transaction_id = 0;
                                $walletTransactionDB->transfer_id = $transferredTransaction['transactionId'];
                                $walletTransactionDB->bank_name = $transferredTransaction['responseData'];
                                $walletTransactionDB->bank_id = $userBankAccount->id;
                                $walletTransactionDB->type = 1;
                                $walletTransactionDB->amount = $amount;
                                $walletTransactionDB->balance = $remainingBalance;
                                $walletTransactionDB->description = "Withdraw amount from wallet";
                                $walletTransactionDB->save();

                                $response = array(
                                    'status' => true,
                                    'message' => "Money transferred successfully",
                                );
                                return $response;
                            }
                            else{
                                $response = array(
                                    'status' => false,
                                    'message' =>$transferredTransaction['stripeError'],
                                );
                                return $response;
                            }

                        } catch(\Exception $e) {
                            $body = $e->getMessage(); 
                            $response = array(
                                'status' => 0,
                                'message' =>$body,
                            );
                            return $response;
                        }
                    }
                    else{
                        return array("status" => false, "message" => "Bank account not found!!");
                    }
                }
                else{
                    return array("status" => false, "message" => "Insufficient funds in your wallet account.");
                }
            }
            else{
                return array("status" => false, "message" => "Wallet not found!!");
            }
        }
        else{
            return array("status" => false, "message" => "User not found!!");
        }
    }

    /**
       * Calculate average rating
    */
    public static function calAvgRating($userId){
        if(Auth::id() == $userId){
            $avgRating = Rating::where('to_user_id', $userId)->avg("ratings");    
        }
        else if(Auth::user()->role == '3'){
            $activeInflue = Self::getActiveInfluencer(Auth::id());
            if($activeInflue){
                if($activeInflue->id == $userId){
                    $avgRating = Rating::where('to_user_id', $userId)->avg("ratings");
                }
                else{
                    $avgRating = Rating::where('to_user_id', $userId)
                                /*->where(function($query) {
                                    $query->where('rating_approval_status', '1')
                                    ->orWhereIn('admin_status', ['2','3']);
                                })*/
                                ->avg("ratings");
                }
            }
            else{
                $avgRating = Rating::where('to_user_id', $userId)
                                /*->where(function($query) {
                                    $query->where('rating_approval_status', '1')
                                    ->orWhereIn('admin_status', ['2','3']);
                                })*/
                                ->avg("ratings");
            }
        }
        else{
            $avgRating = Rating::where('to_user_id', $userId)
                                /*->where(function($query) {
                                    $query->where('rating_approval_status', '1')
                                    ->orWhereIn('admin_status', ['2','3']);
                                })*/
                                ->avg("ratings");
        }
        
        return round(2*$avgRating)/2;
    }

    /**
       * Total rating
    */
    public static function totalRating($userId){
        if(Auth::id() == $userId){
            $totalRating = Rating::where('to_user_id', $userId)->count();    
        }
        else if(Auth::user()->role == '3'){
            $activeInflue = Self::getActiveInfluencer(Auth::id());
            if($activeInflue){
                if($activeInflue->id == $userId){
                    $totalRating = Rating::where('to_user_id', $userId)->count();
                }
                else{
                    $totalRating = Rating::where('to_user_id', $userId)
                                /*->where(function($query) {
                                    $query->where('rating_approval_status', '1')
                                    ->orWhereIn('admin_status', ['2','3']);
                                })*/
                                ->count();
                }
                
            }
            else{
                $totalRating = Rating::where('to_user_id', $userId)
                                /*->where(function($query) {
                                    $query->where('rating_approval_status', '1')
                                    ->orWhereIn('admin_status', ['2','3']);
                                })*/
                                ->count();
            }
        }
        else{
            $totalRating = Rating::where('to_user_id', $userId)
                                /*->where(function($query) {
                                    $query->where('rating_approval_status', '1')
                                    ->orWhereIn('admin_status', ['2','3']);
                                })*/
                                ->count();
        }
        
        return round($totalRating);
    }

    /**
       * Wallet credit tansaction
    */
    public static function walletCreditTransaction($userId, $amount, $appliedId, $description = "", $cardTransactionId = 0, $otherData = array()){

        $applyCollab = ApplyCollab::find($appliedId);

        $creditedUserId = $userId;
        $agencyId = 0;

        $linkedaccountheader = AgencyInfluencer::where('influencer_id',$creditedUserId)->where('is_linked',1)->first();

        if($linkedaccountheader){
            $creditedUserId = $linkedaccountheader->agency_id;            
            $agencyId = $linkedaccountheader->agency_id;            
        }

        $walletBalanceDB = WalletBalance::where("user_id",$creditedUserId)->first();
        if($walletBalanceDB){
            $walletBalanceDB->balance = $walletBalanceDB->balance + $amount;
            $walletBalanceDB->save();
        }
        else{
            $walletBalanceDB = new WalletBalance();
            $walletBalanceDB->balance = $amount;
            $walletBalanceDB->user_id = $creditedUserId;
            $walletBalanceDB->save();
        }

            $walletTransactionDB = new WalletTransaction();
            $walletTransactionDB->user_id = $creditedUserId;
            $walletTransactionDB->card_transaction_id = $cardTransactionId;
            $walletTransactionDB->type = 0;
            $walletTransactionDB->amount = $amount;
            $walletTransactionDB->balance = $walletBalanceDB->balance;
            $walletTransactionDB->description =  $description;
            $walletTransactionDB->save();

            if($linkedaccountheader){
                $linkedaccountheader->agency->notify(new \App\Notifications\WalletTransferred($walletTransactionDB));
            }
            else{
                $userDB = User::find($userId);
                $userDB->notify(new \App\Notifications\WalletTransferred($walletTransactionDB));
            }

        $paymentReceiptArr = [
            "agency_id" => $agencyId,
            "influencer_id" => $userId,
            "brand_id" => $applyCollab->collab->user_id,
            "applied_id" => $appliedId,
            "transaction_id" => $walletTransactionDB->id,
            "card_transaction_id" => $cardTransactionId,
            "amount" => isset($otherData['full_amount']) ? $otherData['full_amount'] : $amount,
            "dispute_id" => isset($otherData['dispute_id']) ? $otherData['dispute_id'] : 0,
            "cancel_dispute_id" => isset($otherData['cancel_dispute_id']) ? $otherData['cancel_dispute_id'] : 0,
            "refund_amount" => isset($otherData['refund_amount']) ? $otherData['refund_amount'] : 0,
            "transaction_charge" => isset($otherData['transaction_charge']) ? $otherData['transaction_charge'] : 0,
        ];

        Self::addReceipt($paymentReceiptArr);

        return $walletBalanceDB->balance;

    }

    /**
       * Add receipt
    */
    public static function addReceipt($data = []){
        
        $paymentReceipt = new PaymentReceipt();
        $paymentReceipt->agency_id = isset($data['agency_id']) ? $data['agency_id'] : 0;
        $paymentReceipt->influencer_id = isset($data['influencer_id']) ? $data['influencer_id'] : 0;
        $paymentReceipt->brand_id = isset($data['brand_id']) ? $data['brand_id'] : 0;
        $paymentReceipt->applied_id = isset($data['applied_id']) ? $data['applied_id'] : 0;
        $paymentReceipt->transaction_id = isset($data['transaction_id']) ? $data['transaction_id'] : 0;
        $paymentReceipt->amount = isset($data['amount']) ? $data['amount'] : 0;
        $paymentReceipt->dispute_id = isset($data['dispute_id']) ? $data['dispute_id'] : 0;
        $paymentReceipt->cancel_dispute_id = isset($data['cancel_dispute_id']) ? $data['cancel_dispute_id'] : 0;
        $paymentReceipt->refund_amount = isset($data['refund_amount']) ? $data['refund_amount'] : 0;
        $paymentReceipt->transaction_charge = isset($data['transaction_charge']) ? $data['transaction_charge'] : 0;
        $paymentReceipt->card_transaction_id =  isset($data['card_transaction_id']) ? $data['card_transaction_id'] : 0;
        $paymentReceipt->save();

        if($data['transaction_id'] != 0){
            Log::debug('payment reciept added influencer addReceipt -----'.$paymentReceipt->id.'-----'.time());
            $paymentReceipt->influencer->notify(new influencerInvoice($paymentReceipt, 'Collaboration Receipt'));
        }
        $paymentReceipt->brand->notify(new brandInvoice($paymentReceipt, 'Collaboration Receipt'));
        Log::debug('payment reciept added -----'.$paymentReceipt->id.'-----'.time());
    }

    /**
       * Wallet debit transaction
    */
    public static function walletDebitTransaction($userId, $amount, $appliedId, $description = "", $cardTransactionId = 0){

        $applyCollab = ApplyCollab::find($appliedId);

        $walletBalanceDB = WalletBalance::where("user_id",$userId)->where("balance",">=", $amount)->first();
        if($walletBalanceDB){
            $walletBalanceDB->balance = $walletBalanceDB->balance - $amount;
            $walletBalanceDB->save();
        }
        else{
            return ["status" => 0, "message" => "Low balance."];
        }

            $walletTransactionDB = new WalletTransaction();
            $walletTransactionDB->user_id = $userId;
            $walletTransactionDB->card_transaction_id = $cardTransactionId;
            $walletTransactionDB->type = 1;
            $walletTransactionDB->amount = $amount;
            $walletTransactionDB->balance = $walletBalanceDB->balance;
            $walletTransactionDB->description =  $description;
            $walletTransactionDB->save();

        return $walletBalanceDB->balance;

    }

    /**
       * Get day from date
    */
    public static function getDayOfWeek($startDate)
    {
        $today = Carbon::now();
        return $today->diff($startDate)->days;
    }

    /**
       * Get week from date
    */
    public static function getWeekMonth($startDate)
    {
        $today = Carbon::now();
        $diffDays = $today->diff($startDate)->days;
        switch ($diffDays) {
            case ($diffDays <= 7):
                $week = 1;
                break;
            case ($diffDays <= 14):
                $week = 2;
                break;
            case ($diffDays <= 21):
                $week = 3;
                break;
            case ($diffDays <= 28):
                $week = 4;
                break;
            case ($diffDays <= 30):
                $week = 5;
                break;
            default:
                $week = 1;
                break;
        }
        return $week;
    }

    /**
       * Cancel payment refund initiation
    */
    public static function cancelPaymentIntiateRefund($applyCollab){
        $startCollab = $applyCollab->start_collab;
        $lastTransaction = self::getLastTransactionDate($applyCollab->id);
        if (!empty($lastTransaction)) {

            $amount = array();
            if ($startCollab->payment_type == "2") {
                $dayofWeek = self::getDayOfWeek($lastTransaction->created_at);
                $amount = self::getWeeklyCancelPaymentRefundAmount($lastTransaction,$dayofWeek);
            } elseif ($startCollab->payment_type == "3") {
                $weekOfMonth = self::getWeekMonth($lastTransaction->created_at);
                $amount = self::getMonthlyCancelPaymentRefundAmount($lastTransaction,$weekOfMonth);
            } elseif ($startCollab->payment_type == "1") {
                $cancelPaymentDispute = CancelPaymentDispute::where('applied_id',$startCollab->applied_id)->first();
                if (!empty($cancelPaymentDispute->accept_by_other) && $cancelPaymentDispute->accept_by_other == '1') {
                    $amount = array(
                        'influencer_amount' => $lastTransaction->transaction_amount,
                        'brand_amount' => 0,
                        'refund_percentage' => 0,
                    );    
                } else {
                    $amount = array(
                        'influencer_amount' => 0,
                        'brand_amount' => $lastTransaction->transaction_amount,
                        'refund_percentage' => 100,
                    );
                }
            }
            if (!empty($amount)) {
                if ($amount['brand_amount'] > 0) {
                    self::intitateRefund($lastTransaction,$amount['brand_amount']);

                    $lastTransaction->user->notify(new \App\Notifications\CancelPayment\CancelPaymentRefundRecurringToBrand($lastTransaction, $lastTransaction->transaction_amount , $amount['refund_percentage']));

                    if($amount['refund_percentage'] == 100){
                        // Add Receipt entry start
                        $creditedUserId = $applyCollab->user_id;
                        $agencyId = 0;

                        $linkedaccountheader = AgencyInfluencer::where('influencer_id',$creditedUserId)->where('is_linked',1)->first();

                        if($linkedaccountheader){
                            $creditedUserId = $linkedaccountheader->agency_id;            
                            $agencyId = $linkedaccountheader->agency_id;            
                        }
                        $adminCharge = ($amount['brand_amount'] * Config::get('constant.ADMIN_CHARGE')) / 100;

                        $paymentReceiptArr = [
                            "agency_id" => $agencyId,
                            "influencer_id" => $applyCollab->user_id,
                            "brand_id" => $applyCollab->collab->user_id,
                            "applied_id" => $applyCollab->id,
                            "transaction_id" => 0,
                            "amount" => $lastTransaction->transaction_amount,
                            "dispute_id" => 0,
                            "cancel_dispute_id" => 0,
                            "refund_amount" => $amount['brand_amount'],
                            "transaction_charge" => $adminCharge,
                            "card_transaction_id" => $lastTransaction->id,
                        ];

                        self::addReceipt($paymentReceiptArr);

                        // Add Receipt entry end
                    }

                }
                if ($amount['influencer_amount'] > 0) {
                    $adminCharge = ($amount['brand_amount'] * Config::get('constant.ADMIN_CHARGE')) / 100;

                    $otherData = [
                        "refund_amount" => $amount['brand_amount'],
                        "transaction_charge" => $adminCharge,
                        "full_amount" => $lastTransaction->transaction_amount
                    ];

                    self::creditAmount($lastTransaction,$amount['influencer_amount'], $otherData);
                }
            }
        }
    }

    /**
       * Initiate refund
    */
    public static function intitateRefund($lastTransaction,$amount)
    {
        $adminCharge = ($amount * Config::get('constant.ADMIN_CHARGE')) / 100;
        $amount = $amount - $adminCharge;

        $refundData = StripeHelper::refundCharge($lastTransaction->transaction_id, $amount);
        $transactionDB = CardTransaction::where("applied_id", $lastTransaction->applied_id)
        ->where("is_transferred",'0')
        ->where("transaction_type",'0')
        ->where("transaction_status",'1')
        ->orderBy('id', 'DESC')
        ->first();

        $transactionDB->refund_amount = $lastTransaction->transaction_amount;   
        $transactionDB->refund_transaction_meta = $refundData['chargeData'];   
        $transactionDB->transaction_type = '1';
        $transactionDB->save();
    }

    /**
       * Credit amount in wallet
    */
    public static function creditAmount($lastTransaction,$amount, $otherData = array())
    {
        if ($lastTransaction->getApplyCollab->start_collab->payment_type == '1') {
            $discription = "One time payment transferred from ".$lastTransaction->user->name;
        } elseif ($lastTransaction->getApplyCollab->start_collab->payment_type == '2') {
            $discription = "Weekly payment transferred from ".$lastTransaction->user->name;
        } elseif ($lastTransaction->getApplyCollab->start_collab->payment_type == '3') {
            $discription = "Monthly payment transferred from ".$lastTransaction->user->name;
        }
        Helper::walletCreditTransaction($lastTransaction->getApplyCollab->user_id, $amount, $lastTransaction->getApplyCollab->id, $discription, $lastTransaction->id, $otherData);

        $lastTransaction->is_transferred = '1';
        $lastTransaction->save();
    }

    /**
       * Cancel payment refund for monthly recurring collab
    */
    public static function getMonthlyCancelPaymentRefundAmount($lastTransaction,$weekOfMonth){
        $influencer_amount = 0;
        $brand_amount = 0;
        $refund_percentage = 0;
        $amount = $lastTransaction->transaction_amount;
        if ($weekOfMonth == 1) {
            $influencer_amount =  0;
            $brand_amount = $amount;
            $refund_percentage = 100;
        } elseif ($weekOfMonth == 2) {
            $influencer_amount =  $amount*0.25;
            $brand_amount = $amount*0.75;
            $refund_percentage = 75;
        } elseif ($weekOfMonth == 3) {
            $influencer_amount = $amount/2;
            $brand_amount = $amount/2;            
            $refund_percentage = 50;
        } elseif ($weekOfMonth == 4) {
            $influencer_amount =  $amount*0.75;
            $brand_amount = $amount*0.25;
            $refund_percentage = 25;
        } elseif ($weekOfMonth == 5) {
            $influencer_amount = $amount;
            $brand_amount = 0;
            $refund_percentage = 0;
        }
        $finalAmount = array(
            'influencer_amount' => $influencer_amount,
            'brand_amount' => $brand_amount,
            'refund_percentage' => $refund_percentage,
        );
        return $finalAmount;
    }

    /**
       * Cancel payment refund for weekly recurring collab
    */
    public static function getWeeklyCancelPaymentRefundAmount($lastTransaction,$dayofWeek){
        $amount = $lastTransaction->transaction_amount;
        $influencer_amount = 0;
        $brand_amount = 0;
        $refund_percentage = 0;
        if ($dayofWeek <= 2) {
            $influencer_amount =  0;
            $brand_amount = $amount;  
            $refund_percentage = 100;
        } elseif ($dayofWeek <= 5) {
            $influencer_amount = $amount/2;
            $brand_amount = $amount/2;
            $refund_percentage = 50;
        } elseif ($dayofWeek <= 7) {
            $influencer_amount = $amount;
            $brand_amount = 0;
            $refund_percentage = 0;
        }
        $finalAmount = array(
            'influencer_amount' => $influencer_amount,
            'brand_amount' => $brand_amount,
            'refund_percentage' => $refund_percentage,
        );
        return $finalAmount;
    }

    /**
       * Get last transaction date
    */
    public static function getLastTransactionDate($applyId)
    {
        $lastTransaction = CardTransaction::where('applied_id',$applyId)->where('transaction_status','1')->where('is_transferred','0')->where('transaction_type','0')->orderBy('id','desc')->first();
        return $lastTransaction;
    }

    /**
       * Check influencer linked with agency or not
    */
    public static function isLinked($user_id=false){
        if (!$user_id) {
            $user_id = Auth::user()->id;
        }
         $linkedaccountheader = \App\Models\AgencyInfluencer::where('influencer_id',$user_id)->where('is_linked',1)->get();
        if ($linkedaccountheader->count()  == 0) {
            return false;
        } else {
            return true;
        }
    }

    /**
       * Check agency linked with influencer or not
    */
    public static function isLinkedNotLinked(){
        $influencer = AgencyInfluencer::where('agency_id',Auth::user()->id)->where('is_active','=',1)->first();
        if (empty($influencer)) {
            return false;
        } else {
            return true;
        }
    }

    /**
       * Send referral notification
    */
    public static function sendReferralNotification($userId)
    {
        $user = User::find($userId);
        if($user){
            if($user->referral_code){
                $referralUsers = User::where('own_referral_code',$user->referral_code)->first();
                if($referralUsers){
                    $referralUsers->notify(New JoinedViaReferral($user));
                }
            }
        }
    }

    /**
       * Free subscription
    */
    public static function freeSubscription($userId){
        $user = User::find($userId);

        FirebaseHelper::updateUserExpireStatus($user, true);

        $launchDate = Config::get('constant.APP_LAUNCH_DATE');

        $dayDiff = $user->created_at->diffInWeeks($launchDate);

        $plan = Plan::where("amount",'0')->first();

        $start_date = Carbon::now()->format("Y-m-d H:i:s");
        if($dayDiff > 1){
            $end_date = Carbon::now()->addMonth(1)->format("Y-m-d H:i:s");
        }
        else{
            $end_date = Carbon::now()->addMonth(3)->format("Y-m-d H:i:s");
        }
        
        $subscriptionDB = new Subscription();
        $subscriptionDB->user_id = $userId;
        $subscriptionDB->plan_id = $plan->id;
        $subscriptionDB->start_date = $start_date;
        $subscriptionDB->end_date = $end_date;
        $subscriptionDB->status = 1;
        $subscriptionDB->is_expired = 0;
        $subscriptionDB->save();
    }

    /**
       * Get users active plan
    */
    public static function getActivePlan($userId){
        $subscriptionDB = Subscription::where("user_id", $userId)
            ->whereIn('status',[1,2])
            ->where("end_date",">=",Carbon::now())
            ->first();

        return $subscriptionDB;
    }

    /**
       * Get users latest plan
    */
    public static function getLatestPlan($userId){
        $existingSubscription = Subscription::where("user_id",$userId)->orderBy("id","DESC")->first();

        return $existingSubscription;
    }

    /**
       * Get users upcoming plan
    */
    public static function getUpcomingPlan($userId){
        $subscriptionDB = Subscription::where("user_id", $userId)
                ->where(function($q){
                    $q->where("status","0")
                    ->orWhere("start_date",">=",Carbon::now());
                })
                ->first();

        return $subscriptionDB;
    }

    /**
       * Get user free plan
    */
    public static function getFreePlan($userId){
        $freeSubscription = Subscription::where("user_id",$userId)->where("plan_id","1")->orderByDesc("id")->first();
        return $freeSubscription;
    }

    /**
       * Check subscription expired or not
    */
    public static function isSubscriptionExpired($userId){
        $latestSubscription = Subscription::where("user_id",$userId)->orderBy("id","DESC")->first();
        
        if($latestSubscription){
            $endDate = new Carbon($latestSubscription->end_date);
            if($endDate->format('Y-m-d') < date("Y-m-d") || $latestSubscription->status == 4 || $latestSubscription->status == 3)
            {
                return true;
            }
            else{
                return false;
            }
        }
        else{
            return true;
        }
    }

    /**
       * Duspute refund to brand
    */
    public static function disputeRefundBrand($dispute){
        $transactionDB = CardTransaction::where("applied_id", $dispute->applied_id)
            ->where("is_transferred",'0')
            ->where("transaction_type",'0')
            ->where("transaction_status",'1')
            ->orderBy('id', 'DESC')
            ->first();
        if($transactionDB){
            $appId = ApplyCollab::find($dispute->applied_id);
            $appId->collab_status = "2";
            $appId->save();

            $adminCharge = ($transactionDB->transaction_amount * Config::get('constant.ADMIN_CHARGE')) / 100;
            $refundedAmount = $transactionDB->transaction_amount - $adminCharge;

            $refundData = StripeHelper::refundCharge($transactionDB->transaction_id, $refundedAmount);

            $transactionDB->refund_amount = $refundedAmount;   
            $transactionDB->refund_transaction_meta = $refundData['chargeData'];   
            $transactionDB->transaction_type = '1';
            $transactionDB->save();
            
            $notifyInfluecer = $dispute->otherUser;
            $notifyInfluecer->notify(new DisaputeAutoResolvedInfluencer($dispute));

            $notifyBrand = $dispute->user;
            $notifyBrand->notify(new DisaputeAutoResolvedBrand($dispute));

            $notifyBrand->notify(New CompleteCollaborationBrand($dispute->applyCollab));
            $notifyInfluecer->notify(New CompleteCollaborationInfluencer($dispute->applyCollab));

            return ["status" => true];
        }
        else{
            return ["status" => false];
        }
    }

    /**
       * Dispute amount paid to influencer
    */
    public static function disputeInfluencerPaid($dispute){
        $transactionDB = CardTransaction::where("applied_id", $dispute->applied_id)
            ->where("is_transferred",'0')
            ->where("transaction_type",'0')
            ->where("transaction_status",'1')
            ->orderBy('id', 'DESC')
            ->first();

        if($transactionDB){
            $appId = ApplyCollab::find($dispute->applied_id);
            $appId->collab_status = "2";
            $appId->save();

            $description = "Dispute of ".$transactionDB->user->name." has been reject by ".Config::get('constant.APP_SIMPLE_NAME');
            Self::walletCreditTransaction($transactionDB->getApplyCollab->user_id, $transactionDB->transaction_amount, $dispute->applied_id, $description, $transactionDB->id);
        
            $transactionDB->is_transferred = '1';
            $transactionDB->save();

            $notifyInfluecer = $dispute->otherUser;
            $transaction_amount = $transactionDB->transaction_amount;

            $notifyInfluecer->notify(new DisputeEscalateAutoCompleteInfluencerReceivedAmount($dispute,$transaction_amount));
            $notifyInfluecer->notify(New \App\Notifications\CompleteCollaborationInfluencer($appId));
            $notifyBrand = $dispute->user;


            $notifyBrand->notify(new DisputeEscalateAutoCompleteBrandNotRefund($dispute));
            $notifyBrand->notify(New \App\Notifications\CompleteCollaborationBrand($appId));

            return ["status" => true];
        }
        else{
            return ["status" => false];
        }
    }

    /**
       * Cancel payment dispute amount refund to brand
    */
    public static function disputeRefundBrandCancelPayment($dispute){
        $transactionDB = CardTransaction::where("applied_id", $dispute->applied_id)
            ->where("is_transferred",'0')
            ->where("transaction_type",'0')
            ->where("transaction_status",'1')
            ->orderBy('id', 'DESC')
            ->first();
        if($transactionDB){
            $appId = ApplyCollab::find($dispute->applied_id);
            $appId->collab_status = "2";
            $appId->save();

            $adminCharge = ($transactionDB->transaction_amount * Config::get('constant.ADMIN_CHARGE')) / 100;
            $refundedAmount = $transactionDB->transaction_amount - $adminCharge;

            $refundData = StripeHelper::refundCharge($transactionDB->transaction_id, $refundedAmount);

            $transactionDB->refund_amount = $refundedAmount;   
            $transactionDB->refund_transaction_meta = $refundData['chargeData'];   
            $transactionDB->transaction_type = '1';
            $transactionDB->save();
            
            $notifyInfluecer = $dispute->user;
            $notifyInfluecer->notify(new DisaputeAutoResolvedInfluencerCancelPayment($dispute));

            $notifyBrand = $dispute->otherUser;
            $notifyBrand->notify(new DisaputeAutoResolvedBrandCancelPayment($dispute));

            $notifyBrand->notify(New CompleteCollaborationBrand($dispute->applyCollab));
            $notifyInfluecer->notify(New CompleteCollaborationInfluencer($dispute->applyCollab));

            return ["status" => true];
        }
        else{
            return ["status" => false];
        }
    }

    /**
       * Cancel payment amount paid to influencer
    */
    public static function disputeInfluencerPaidCancelPayment($dispute){
        $transactionDB = CardTransaction::where("applied_id", $dispute->applied_id)
            ->where("is_transferred",'0')
            ->where("transaction_type",'0')
            ->where("transaction_status",'1')
            ->orderBy('id', 'DESC')
            ->first();

        if($transactionDB){
            $appId = ApplyCollab::find($dispute->applied_id);
            $appId->collab_status = "2";
            $appId->save();

            $description = "Dispute of ".$transactionDB->user->name." has been reject by ".Config::get('constant.APP_SIMPLE_NAME');
            Self::walletCreditTransaction($transactionDB->getApplyCollab->user_id, $transactionDB->transaction_amount, $dispute->applied_id, $description, $transactionDB->id);
        
            $transactionDB->is_transferred = '1';
            $transactionDB->save();

            $notifyInfluecer = $dispute->user;
            $transaction_amount = $transactionDB->transaction_amount;

            $notifyInfluecer->notify(new DisputeEscalateAutoCompleteInfluencerReceivedAmountCancelPayment($dispute,$transaction_amount));
            $notifyInfluecer->notify(New \App\Notifications\CompleteCollaborationInfluencer($appId));
            $notifyBrand = $dispute->otherUser;


            $notifyBrand->notify(new DisputeEscalateAutoCompleteBrandNotRefundCancelPayment($dispute));
            $notifyBrand->notify(New \App\Notifications\CompleteCollaborationBrand($appId));

            return ["status" => true];
        }
        else{
            return ["status" => false];
        }
    }

    /**
       * Block user
    */
    public static function blockUser($appliedId, $userId, $action){
        $applyCollabDB = ApplyCollab::find($appliedId);

        if($applyCollabDB->user_id == $userId){
            $blockUserId = $applyCollabDB->collab->user->u_id;
            $blocked_to = $applyCollabDB->collab->user->id;
        }
        else{
            $blocked_to = $applyCollabDB->influencer->id;
            $blockUserId = $applyCollabDB->influencer->u_id;   
        }

        if($action == 0){
            $activeCollab = ApplyCollab::where("user_id", $applyCollabDB->user_id)
                            ->where("collab_user_id", $applyCollabDB->collab_user_id)
                            ->where("room_id",'!=' ,'')
                            ->get();

            BlockedUser::where(['blocked_from' => $userId,'blocked_to' => $blocked_to])->delete();
            foreach($activeCollab as $activeColl){
                FirebaseHelper::blockUser($blockUserId, $activeColl->room_id, $action);
            }

            $response = array(
                'message' =>  Self::getNotificationMessage('user_unblock_success'),
                'status' => 1,
            );
            return $response;
        }
        else{
            $activeCollabCount = ApplyCollab::where("user_id", $applyCollabDB->user_id)
                                ->where("collab_user_id", $applyCollabDB->collab_user_id)
                                ->whereIn("collab_status", ['1','3'])
                                ->count();

            if($activeCollabCount != 0){
                $response = array(
                    'message' =>  Self::getNotificationMessage('can_not_block_user'),
                    'status' => 1,
                );
                return $response;
            }
            else{
                BlockedUser::create(['blocked_from' => $userId,'blocked_to' => $blocked_to]);
                $activeCollab = ApplyCollab::where("user_id", $applyCollabDB->user_id)
                            ->where("collab_user_id", $applyCollabDB->collab_user_id)
                            ->where("room_id",'!=' ,'')
                            ->get(); 

                foreach($activeCollab as $activeColl){
                    FirebaseHelper::blockUser($blockUserId, $activeColl->room_id, $action);
                }

                $response = array(
                    'message' =>  Self::getNotificationMessage('user_block_success'),
                    'status' => 1,
                );
                return $response;
            }     
        }
    }

    /**
       * Get block status
    */
    public static function getBlockedStatus($currentUserId,$otherUserId)
    {
        $blockedType1 = BlockedUser::where([
            'blocked_from' => $currentUserId,
            'blocked_to'   => $otherUserId,
        ])->count();
        $blockedType2 = BlockedUser::where([
            'blocked_from' => $otherUserId,
            'blocked_to'   => $currentUserId,
        ])->count();
        $response = array();
        if ($blockedType1 > 0 && $blockedType2 > 0) {
            // blocked each other
            $response = array(
                'message' => 'You both have blocked each other.',
                'blocked_type' => 3,
            );
        } elseif ($blockedType1 > 0) {
            //blocked by current user
            $response = array(
                'message' => 'You blocked this contact.',
                'blocked_type' => 1,
            );
        } elseif ($blockedType2 > 0) {
            //blocked by other user
            $response = array(
                'message' => 'You have been blocked by this contact.',
                'blocked_type' => 1,
            );
        } else {
            //not blocked by any user
            $response = array(
                'message' => '',
                'blocked_type' => 0,
            );
        }
        return $response;
    }

    /**
       * Update email verify
    */
    public static function updateEmailVerify($role,$userId,$email)
    {
        $user = User::find($userId);
        $post = [];
        if ($role == 1) {
            $post['verifyLink'] = route('influencer.verify-account',$user->id);    
        } elseif ($role == 2) {
            $post['verifyLink'] = route('brand.verify-account',$user->id);
        } elseif ($role == 3) {
            $post['verifyLink'] = route('agency.verify-account',$user->id);
        }
        
        $post['email'] = $email;
        $post['userName'] = $user->name;
        $post['userid'] = $userId;
        $post['subject'] = "Email Changed - ".Config::get('constant.APP_SIMPLE_NAME');
        $template = 'updateEmailVerify';

        if (!empty($post)) {
            $result = self::sendMail($post,$template);
            return $result;
        }else{
            return false;
        }
       
    }
    public static function processAgeGroup($ageGroup)
    {
        $totalCount = array_sum($ageGroup);
        $ageGroupMain = array(
            '0-17' => 0,
            '18-24' => 0,
            '25-34' => 0,
            '35+' => 0,
        );
        $maleFemaleRatio = array(
            'M' => 0,
            'F' => 0,
        );
        foreach ($ageGroup as $key => $value) {
            $key = explode('.', $key);
            $gender = $key[0];
            if ($gender != 'M' && $gender != 'F') {
                $gender = 'F';
                
            }
            $explodeData = explode('-', $key[1]);
            $ageGroupKey = isset($explodeData[1]) ? $explodeData[1] : 36;
            $maleFemaleRatio[$gender] += $value;
            //echo $ageGroupKey."<br>";
            if ($ageGroupKey > 35) {
              $ageGroupMain['35+'] += $value;  
            } elseif ($ageGroupKey > 25) {
              $ageGroupMain['25-34'] += $value;  
            } elseif ($ageGroupKey > 18) {
              $ageGroupMain['18-24'] += $value;  
            } else{
              $ageGroupMain['0-17'] += $value;
            }
        }
        //dd($ageGroupMain);
        if ($totalCount > 0) {
            $ageGroupMain = array_map(function($v) use ($totalCount){
                return round(($v/$totalCount)*100,1);
            }, $ageGroupMain);
            $maleFemaleRatio = array_map(function($v) use ($totalCount){
                return round(($v/$totalCount)*100,1);
            }, $maleFemaleRatio);

        }
        $response = array(
            'ageGroup' => $ageGroupMain,
            'maleFemaleRatio' => $maleFemaleRatio,
            'totalCount' => $totalCount,
        );
        return $response;
    }

    /**
       * get users engagement data 
    */
    public static function getUserEngagmentDataAPI($userId,$socialMediaId=false)
    {
        if (!$socialMediaId) {

            $socialMedia = UserSocialmedia::where('user_id',$userId)->whereIn('socialmedia_id',[1,2])->where('is_linked',1)->get();

            $response = array();
            foreach ($socialMedia as $key => $value) {
                $tempData = array();
                $tempData['socialMediaId']         = $value->socialmedia_id;
                $tempData['socialMediaName']       = $value->socialmedia->name;   
                $tempData['followrsCount']        = getFollowerCount($value->follwers);   
                $tempData['likesCount']            = getFollowerCount($value->like_count);   
                $tempData['commentsCount']         = getFollowerCount($value->comment_count);   
                $tempData['engagementRatio']       = getRatio($value->engagement_ratio,true);   
                $tempData['maleAvg']               = getRatio($value->male_ratio,true);   
                $tempData['femaleAvg']             = getRatio($value->female_ratio,true);   
                $tempData['agegroup']['age0to17']  = getRatio($value->age_group_0_17,true);   
                $tempData['agegroup']['age18to24'] = getRatio($value->age_group_18_24,true);   
                $tempData['agegroup']['age25to34'] = getRatio($value->age_group_25_34,true);   
                $tempData['agegroup']['age35+']    = getRatio($value->age_group_35_plus,true);   
                
                $response[] = $tempData;
            }
            return $response;
        } else {
            $socialMedia = UserSocialmedia::where('user_id',$userId)->where('socialmedia_id',$socialMediaId)->where('is_linked',1)->first();
            $tempData = array();
            if (!empty($socialMedia)) {
                $tempData['socialMediaId']         = $socialMedia->socialmedia_id;
                $tempData['socialMediaName']       = $socialMedia->socialmedia->name;   
                $tempData['followrsCount']        = getFollowerCount($socialMedia->follwers);   
                $tempData['likesCount']            = getFollowerCount($socialMedia->like_count);   
                $tempData['commentsCount']         = getFollowerCount($socialMedia->comment_count);   
                $tempData['engagementRatio']       = getRatio($socialMedia->engagement_ratio,true);   
                $tempData['maleAvg']               = getRatio($socialMedia->male_ratio,true);   
                $tempData['femaleAvg']             = getRatio($socialMedia->female_ratio,true);   
                $tempData['agegroup']['age0to17']  = getRatio($socialMedia->age_group_0_17,true);   
                $tempData['agegroup']['age18to24'] = getRatio($socialMedia->age_group_18_24,true);   
                $tempData['agegroup']['age25to34'] = getRatio($socialMedia->age_group_25_34,true);   
                $tempData['agegroup']['age35+']    = getRatio($socialMedia->age_group_35_plus,true);   
            }
            return $tempData;
        }
    }

}