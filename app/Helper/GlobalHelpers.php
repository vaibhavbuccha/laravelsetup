<?php
function urlCustomEncode($parameter)
{
    return base64_encode($parameter);
}
function urlCustomDecode($parameter)
{
    return base64_decode($parameter);
}
function dateFormat($date, $time = false)
{
	$date = new \Carbon\Carbon($date);
	if($time){
		return $date->format("d F Y h:i:s A");
	}
	else{
		return $date->format("d F Y");
	}
}

function clickAction($notifiable, $notificationType = "")
{
	if($notifiable->role == '1'){
		return url("influencer/notifications");
	}
	elseif($notifiable->role == '2'){
		return url("brand/notifications");
	}
	else{
		return url("agency/notifications");
	}
}

function amountDisplayFormat($amount){
	return round($amount,2);
}
function getFcmNotificationArray($notifiable,$title,$type)
{
	if ($notifiable->device_type == "2") {
		return [];
	} else {
		return [
	            'title'        => Config::get('constant.REPLAY_NAME'), 
	            'body'         => $title, 
	            'sound'        => 'default', // Optional 
	            'icon'         => url(Config::get('constant.NOTIFICATION_LOGO')), // Optional
	            'click_action' =>  clickAction($notifiable), // Optional
	            'notificationType' => $type,
	        ];

	}
}
function getFcmDataArray($notifiable,$title,$type)
{
	return	[
            'title'        => Config::get('constant.REPLAY_NAME'), 
            'body'         => $title, 
            'sound'        => 'default', // Optional 
            'icon'         => url(Config::get('constant.NOTIFICATION_LOGO')), // Optional
            'click_action' =>  clickAction($notifiable), // Optional
            'notificationType' => $type,            
        ];
}

function getDefaultCurrencyName(){
	return Config::get('constant.APP_CURRENCY');
}
function getNotificationMessage($type,$data=array())
{
	$message = Config::get('constant.NOTIFICATION_MESSAGE_HTML.'.$type);
	foreach ($data as $key => $value) {
		$message = str_ireplace('{{'.$key.'}}', $value, $message);
	}
	return $message;
}
function getNotificationMessagePlain($type,$data=array())
{
	$message = Config::get('constant.NOTIFICATION_MESSAGE_PLAIN.'.$type);
	foreach ($data as $key => $value) {
		$message = str_ireplace('{{'.$key.'}}', $value, $message);
	}
	return $message;
}
function getUserName($user)
{
	return trim($user->name.' '.$user->last_name);
}

function createReferralCode($user)
{
	$permitted_number = '0123456789';
	$permitted_chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';

	$r1 = substr(str_shuffle($permitted_number), 0, 3);
	$r2 = substr(str_shuffle($permitted_chars), 0, 3);

	return  $r1 . $user->id . $r2;
}
