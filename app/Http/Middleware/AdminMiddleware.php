<?php

namespace App\Http\Middleware;

use Closure;

use Illuminate\Support\Facades\Auth;


class AdminMiddleware
{
    /**
     * Get the path the user should be redirected to when they are not authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return string|null
     */
    public function handle($request, Closure $next)
    {
        $guard = 'admin';
        if(!Auth::guard($guard)->check()){
            return redirect()->route('adminLoginFrom');
        }
        else{
            return $next($request);
        }
    }
}
