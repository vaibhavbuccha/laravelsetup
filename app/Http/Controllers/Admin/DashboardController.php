<?php
/**
    * Author Name: LS
    * Datetime: 2021-08-16
    * Dashboard Controller class for dashboard 'Admin' section
**/

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use App\Helper\Helper;
use Redirect;
use DB;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Str;
use Carbon\Carbon;
use Image;
use Storage;
use Config;

class DashboardController extends Controller
{
	protected $breadcrumb = "Dashboard";
    protected $pageTitle = "Dashboard";

    public function index(Request $request){

    	$breadcrumb = $this->breadcrumb;
        $pageTitle = $this->pageTitle;

        $totalUser = 10;
        $totalProduct = 4;

        // $totalClients = Clients::count();

        return view("admin.dashboard",compact('totalProduct','totalUser','pageTitle'));
    }


    




}
