<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\AllUsersExport;
// use App\Models\Country;
// use App\Models\City;
use Redirect;
use Yajra\Datatables\Datatables;
use File;

class UserController extends Controller
{
    
    public function index()
    {
        $pageTitle = "User List";
        $users = User::orderBy('id', 'DESC')->where('is_active', '=', 1)->get();  
        // dd($users);
        return view('admin.userManagement.index', compact('users', 'pageTitle'));
    }


    public function getalldata()
    {
        if(isset($_REQUEST['order'])){
            $users = User::get();
        }
        else{
            $users = User::orderBy('id', 'DESC')->get(); 
        } 
        
        return Datatables::of($users)
                ->addIndexColumn()
                ->editColumn('name', function($users){
                    $fullName = getUserName($users);
                    $profileUrl = ($users->profile_pic) ? url('uploads/profile_pics/'.$users->profile_pic) : url('uploads/profile_pics/default-avatar.png');
                    $profile = '<a class="avatar" href="javascript:void(0)">
                        <img class="img-fluid" src="'.$profileUrl.'"></a>';

                    return $profile.$fullName;

                })->editColumn('email', function($users){
                    $email = isset($users->email)?trim($users->email):"N/A";
                    return $email;
                })->editColumn('dateofbirth', function($users){
                    $birthdate = isset($users->dob) ? dateFormat($users->dob) : "N/A";
                    return $birthdate;
                })
                ->addColumn('status', function($users){
                    $email = ($users->is_active == 1) ? '<span class="badge badge-success">Active</span>' : '<span class="badge badge-danger">Inactive</span>';
                    return $email;
                })    
                ->setRowClass('viewInformation')
                ->setRowAttr([
                    'data-id' => function($user) {
                        return $user->id;
                    },
                    'data-url' => function($user) {
                        return url("admin/user-management/".$user->id);
                    },
                ])
                ->rawColumns(['name','status'])
                ->make(true);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    public function show($id)
    {
        $page = "User Details";

        $user = User::where('id',$id)->first();
        // $countries = Country::all();
        // $citys = City::where('country_id',$user->country)->orderBy('id','ASC')->get();
        if($user){
            return view('admin.userManagement.view', compact('user','page'));
        }
        else{
            return view('admin.layouts.includes.modalError');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    
    public function update(Request $request, $id)
    {
        $user = User::find($id);

        $page = "User Details";
        if($user){
            $updateData = [
                "name" => $request->name,
                "last_name" => $request->last_name,
                "dob" => $request->dob,
                "country" => $request->country,
                "city"  => $request->city,
            ]; 

            if($request->user_profile_pic){
                $imageMainPath = '/uploads/profile_pics/';
                $imageThumbPath = $imageMainPath.'/thumbnail/';
                if (!is_dir(public_path($imageMainPath))) {
                    mkdir(public_path($imageMainPath), 777, true);         
                }
                if (!is_dir(public_path($imageThumbPath))) {
                    mkdir(public_path($imageThumbPath), 777, true);         
                }

                $profile_image = "user-pro-".time().".png";
                $path = public_path($imageMainPath.$profile_image);
                
                $image = $request->user_profile_pic;  // your base64 encoded
                $image = str_replace('data:image/png;base64,', '', $image);
                $image = str_replace(' ', '+', $image);
                \File::put($path, base64_decode($image));

                $updateData['profile_pic'] = $profile_image;
            }

            User::where("id",$id)->update($updateData);

            return Redirect::to("admin/user-management")->with("success","User details has been updated successfully.");
        }
        else{
            return Redirect::to("admin/user-management")->with("error","Something went wrong. Please try again!!");
        }
    }

   
    public function destroy($id)
    {
        $userDB = User::find($id);

        if($userDB)
        {
            $userDB->delete();

            return redirect()->back()->with("success","User has been deleted successfully.");
        }
        else
        {
            return redirect()->back()->with("error","Opps!! Something went wrong. Please try again.");
        } 
    }


    
    public function exportUser()
    {
      return Excel::download(new AllUsersExport, 'AllUsers-'.date('d-m-Y').'.csv');
  
    }
    public function inactiveUser($id)
    {
        $updateData = [ 'is_active' => 0, ];
        User::where("id",$id)->update($updateData);
        return Redirect::back()->with("success","User Inactive successfully");
                        
    }
    public function activeUser($id)
    {
        $updateData = [ 'is_active' => 1, ];
        User::where("id",$id)->update($updateData);
        return Redirect::back()->with("success","User active successfully");
                        
    }

    
}
