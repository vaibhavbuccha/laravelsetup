<?php

namespace App\Http\Controllers\API\V1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Controllers\API\V1\BaseController as BaseController;
use Tymon\JWTAuth\Exceptions\JWTException;
use JWTAuth;
use App\Models\User;
use Auth;
use Carbon\Carbon; 
use Validator;
use Config;
use Hash;
use App\Helper\Helper;
use Image;
use DB;

class UserController extends BaseController
{
    
    /*
	* Author Name (BS)
	* Datetime (2021-08-19)
	* signUp User
	*/
	public function signUp(Request $request){
	    try{
	        $validator = Validator::make($request->all(), [
	            'firstName' => 'required',
	            'lastName' => 'required',
	            'DOB' => 'required',
	            'country' => 'required',
	            'city' => 'required',
	            'email' => 'required|email|unique:users,email,NULL,id,deleted_at,NULL',
	            'password' => 'required|min:6',
	            'deviceType' => 'required',
	            'deviceToken' => 'required',
	            'loginType' => 'required',
	        ]);
	        
	        if ($validator->fails()) {
	            return $this->sendError(implode(" ", $validator->errors()->all()), null, 401);
	        }
	        if(isset($request->referralCode) && !empty($request->referralCode)){
	            $referralCodeCount = User::where('own_referral_code', $request->referralCode)->whereNotNull('email_verified_at')->count();

	            if($referralCodeCount == 0){
	                $response = array(
	                    'message' =>  'invalid referral code',
	                    'status'  => 0
	                );
	                return Helper::ApiResponse($response);
	            }
	        }
	        $userDB = new User();

	        $userDB->name = $request->firstName;
	        $userDB->last_name = $request->lastName;
	        $userDB->email = $request->email;
	        $userDB->password = bcrypt($request->password);
	        $userDB->dob = $request->DOB;
	        $userDB->country = $request->country;
	        $userDB->city = $request->city;
	        $userDB->referral_code = $request->referralCode;
	        $userDB->is_active = 1;
	        $userDB->login_type = $request->loginType;
	        $userDB->device_type = $request->deviceType;
	        $userDB->device_token = $request->deviceToken;
	        $userDB->login_at = null;
	        $userDB->save();
	 
	        $credentials = $request->only('email', 'password');
	        if (!$token = JWTAuth::attempt($credentials)) {
	            return $this->sendError(Helper::getNotificationMessage('Invalid_credential'), null, 401);
	        }
	       
	        $referralcode = createReferralCode($userDB);
	        $updateArr = [
	            "own_referral_code" => $referralcode
	        ];
	        User::where("id",$userDB->id)->update($updateArr);
	        // End Referral code

	        $emailsArr = [
	        	"email" => $request->email,
	        	"subject" => "Welcome to ".Config::get('constant.APP_NAME'),
	        	"userid" => $userDB->id,
	        	"userName" => getUserName($userDB),
	        ];

	        Helper::sendMail($emailsArr,'userRegisterEmail');

	        $response = [
	            "userId"=> $userDB->id,
	            "userType"=>$request->userType,
	            "email"=> $request->email,
	            "token"=> $token
	        ];

	        $message = Helper::getNotificationMessage('signup_success');
	        
	        return $this->sendResponse($response, $message);
	    }
	    catch(\exception $e){
	        return $this->sendError($e->getMessage(), null, 401);
	    }
	}

	/*
	* Author Name (BS)
	* Datetime (2021-08-19)
	* signIn User
	*/
	public function login(Request $request){
	    try{
	        $validator = Validator::make($request->all(), [
	            'email' => 'required|email',
	            'password' => 'required',
	            'deviceType' => 'required',
	            'deviceToken' => 'required',
	        ]);
	        
	        if ($validator->fails()) {
	            return $this->sendError(implode(" ", $validator->errors()->all()), null, 401);
	        }
	        $userDetails = User::where('email', $request->email)->first();
	        if($userDetails){
	            if($userDetails->is_active == '0'){
	                return $this->sendError(Helper::getNotificationMessage('account_inactive'), null, 401);
	            }
	            else{
	                $credentials = $request->only('email', 'password');
	                
	                if(!$token = JWTAuth::attempt($credentials)) {
	                    return $this->sendError(Helper::getNotificationMessage('Invalid_credential'), null, 401);
	                }
	                
	                $updateArray = [
	                    "device_type" => $request->deviceType,
	                    "device_token" => $request->deviceToken,
	                    "login_at" => null,
	                ];
	                User::where("id",$userDetails->id)->update($updateArray);

	                $response = [
			            "userId"=> $userDetails->id,
			            "userType"=>$userDetails->login_type,
			            "email"=> $userDetails->email,
			            "token"=> $token
			        ];
	                return $this->sendResponse($response, Helper::getNotificationMessage('login_success'));
	            }
	        }
	        else{
	            $message = Helper::getNotificationMessage('user_not_exist');
	            return $this->sendError($message, null, 401);
	        }
	    }
	    catch(\exception $e){
	        return $this->sendError($e->getMessage(), null, 401);
	    }
	}

	/*
	* Author Name (BS)
	* Datetime (2021-08-19)
	* Logout User
	*/
	public function logout(Request $request){
	    try {  
	        $validator = Validator::make($request->all(), [
	          'userId' => 'required',
	          'token' => 'required',
	        ]);

	        if ($validator->fails()) {
	            return $this->sendError(implode(" ", $validator->errors()->all()), null, 401);
	        }

	        $user = User::where('id',$request->userId)->first();
	        
	        if($user)
	        {
	            $updateData = [
	                "device_token" => null,
	                "login_at" => Carbon::now()
	            ]; 
	            
	            User::where("id",$user->id)->update($updateData);

	            JWTAuth::invalidate(JWTAuth::getToken());

	            $response = array(
	                'status' => 1,
	                'message' =>  Helper::getNotificationMessage('logout_success'),
	            );
	        	return $this->sendResponse($response, Helper::getNotificationMessage('logout_success'));
	        }
	        else{
	            return $this->sendError(Helper::getNotificationMessage('user_not_exist'), null, 401);
	        } 
	    }
	    catch(\exception $e){
	        return $this->sendError($e->getMessage(), null, 401);
	    }
	}

	/*
	* Author Name (BS)
	* Datetime (2021-08-19)
	* forgotPassword User 
	*/
	public function forgotPassword(Request $request, $token = null){
	    try {
	        $validator = Validator::make($request->all(), [
	            'email' => 'required',
	        ]);

	        if ($validator->fails()) { 
	            $response = array(
	                'email' => $request->email,
	                'message' => implode(" ", $validator->errors()->all()));
	            return response()->json($response);
	        }

	        $user = User::where('email', $request->email)->where('is_active',1)->first();

	        if(isset($user)){

	                $token = app('auth.password.broker')->createToken($user);

	                $post = [];
	                $post['email'] = $user->email;
	                $post['resetLink'] = url('resetPassword', $token); 
	                $post['subject'] = Config::get('constant.REPLAY_NAME') ."- Forgot password";
	                $post['userName'] = getUserName($user);

	                $result = Helper::sendMail($post,"forgotPassword");
	     
	                if($result['status']){ 
	                    $response = array(
	                        'message' => Helper::getNotificationMessage('forgot_success'),
	                        'status' => 1
	                    );    
	                	return response()->json($response);
	                } 
	                else{
	                    return $this->sendError($result['message'], null, 500);
	                }
	        }else{
	            return $this->sendError(Helper::getNotificationMessage('user_not_exist'), null, 401);
	        }
	    }catch (\exception $e) {
	        return $this->sendError($e->getMessage(), null, 401);
	    }
	}
}
